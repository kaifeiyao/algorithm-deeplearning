# HOW TO START LOCAL
1. 样本集demo(200KB)存放在s3://jiayun-recommend/private/wangqi/data/2019-12-09，需先下载到本地
2. 依赖的第三方库scikit-learn,tensorflow==1.13.1,pandas,boto3

3. 训练模型，以下路径需替换为相应本地路径
python3 deepRecommend/main.py \
    --mode train \
    --feature_conf_path conf/WDconf_1123/feature.yaml \
    --train_data /Users/snoworday/data/wide_deep/traindata/2019-12-09 \
    --eval_data /Users/snoworday/data/wide_deep/traindata/2019-12-09 \
    --graph_path /Users/snoworday/data/wide_deep/experiment/2019-12-09/graph \
    --log_path /Users/snoworday/data/wide_deep/experiment/2019-12-09/logData \
    --result_path /Users/snoworday/data/wide_deep/experiment/2019-12-09/result \
    --online_graph_path /Users/snoworday/data/wide_deep/experiment/2019-12-09/result/online_graph/wide_and_deep_traditional_attention_v3/ \
    --profile_path /Users/snoworday/data/wide_deep/experiment/2019-12-09/profile \
    --tmp_path /tmp/dl_rank
    
    
    