#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin
git clone -b develop https://qiwang1@bitbucket.org/kaifeiyao/algorithm-deeplearning.git
project_dir=$(cd `dirname $0` && pwd)"/algorithm-deeplearning/deepRecommend"

date="2019-12-21"

if [[ -z  "date" ]];then
    date=$(date --date="-1 day" +"%Y-%m-%d")
fi
psNum=5
data_version="base"
experiment_version="test"
conf_path="s3://jiayun-recommend/private/wangqi/wide_deep/data/${date}/${data_version}/Feature/profile.json"

local_ip=$(/sbin/ifconfig | grep 'inet addr:172' | awk '{print $2}' | sed 's/addr://g')
function get_ip_list(){
    clusterId=$(cat /mnt/var/lib/info/job-flow.json | jq -r ".jobFlowId")
    ipList=$(aws emr list-instances --cluster-id ${clusterId} | jq '.Instances' | jq '.[]' -c | jq '.PrivateIpAddress' | tr -d '"')
    echo $(echo ${ipList} | tr ' ' ',')
}
#yarn org.apache.hadoop.yarn.applications.distributedshell.Client -jar /(yarn home)/lib/hadoop-yarn/hadoop-yarn-applications-distributedshell-2.6.0-cdh5.8.0.jar  -shell_command ls

cluster_ips=$(get_ip_list)

function model(){
    cd ${project_dir}

    python3 -u main.py \
       --mode train \
       --partition ${psNum} \
       --version ${experiment_version} \
       --num_ps ${psNum} \
       --local_ip ${local_ip} \
       --cluster_ips ${cluster_ips} \
       --conf_path ${conf_path} \
       --date ${date}
}

function export(){
    cd ${project_dir}

    python3 -u main.py \
       --mode export \
       --version ${experiment_version} \
       --partition ${psNum} \
       --conf_path ${conf_path} \
       --date ${date}
}

echo ${cluster_ips}
function train(){
cd ${project_dir}
model
ret=$?
if [[ $ret -eq 0 ]]
then
    export
fi
}

train
#        "s3://us-west-2.elasticmapreduce/libs/script-runner/script-runner.jar,s3://jiayun-recommend/private/wangqi/wide_deep/train_bash/run_datapipeline.sh"
#
#command-runner.jar,bash,-c,'aws s3 cp s3://jiayun-recommend/private/wangqi/wide_deep/train_bash/run_datapipeline.sh .;./run_datapipeline.sh'