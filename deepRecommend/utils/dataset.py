import tensorflow as tf
from file.wfile import wfile
import os
import math


def _decode(example, type):
    features_config = {}
    features_config['content_seq'] = tf.FixedLenFeature([cfg.max_sequence_length], tf.int64)
    features_config['lookid'] = tf.FixedLenFeature([1], tf.int64)

    base_feature = [
        'content_all_click_rate_smooth',
        'content_all_pic_event_rate_smooth',
        'contentid',
        'darenid'
    ]
    features_config['brandids'] = tf.FixedLenFeature([5], tf.int64)
    features_config['follow_daren_seq'] = tf.FixedLenFeature([50], tf.int64)
    features_config['contentid_content_seq'] = tf.FixedLenFeature([cfg.max_sequence_length], tf.int64)
    features_config['contentid_item_seq'] = tf.FixedLenFeature([cfg.max_sequence_length], tf.int64)
    features_config['darenid_prefer_daren_seq'] = tf.FixedLenFeature([15], tf.int64)
    features_config['tag'] = tf.FixedLenFeature([], tf.string)
    features_config['reweight'] = tf.FixedLenFeature([], tf.float32)
    features_config['page_dwell_time'] = tf.FixedLenFeature([], tf.float32)

    for fea_name in base_feature:
        features_config[fea_name] = tf.FixedLenFeature([], tf.int64)
    if type:
        features_config['label'] = tf.FixedLenFeature([], tf.int64)
        features = tf.parse_example(example, features=features_config)
    else:
        features_config['click_label'] = tf.FixedLenFeature([], tf.int64)
        features = tf.parse_example(example, features=features_config)
        features["label"] = features['click_label']

    label = tf.cast(features['label'], tf.int32)
    click_seq = tf.cast(features['content_seq'], tf.int32)
    lookid = tf.cast(features['lookid'], tf.int32)

    content_content_seq = tf.cast(features['contentid_content_seq'], tf.int32)
    content_item_seq = tf.cast(features['contentid_item_seq'], tf.int32)
    daren_daren_seq = tf.cast(features['darenid_prefer_daren_seq'], tf.int32)
    wide_look = tf.cast(features['contentid'], tf.int32)
    darenid = tf.cast(features['darenid'], tf.int32)
    brandids = tf.cast(features['brandids'], tf.int32)
    follow_daren_seq = tf.cast(features['follow_daren_seq'], tf.int32)
    click_rate = tf.cast(features['content_all_click_rate_smooth'], tf.int32)
    pic_rate = tf.cast(features['content_all_pic_event_rate_smooth'], tf.int32)

    tag = features['tag']
    reweight = features['reweight']
    pdt = features['page_dwell_time']

    return click_seq, lookid, content_content_seq, content_item_seq, daren_daren_seq, wide_look, darenid, brandids, \
           follow_daren_seq, click_rate, pic_rate, label, tag, reweight, pdt


def parse_fn(data_conf, features_conf):
    label_schema = {label: tf.FixedLenFeature([], tf.int64) for label in data_conf.labels}
    feature_schema = {feature_conf.name: tf.FixedLenFeature([feature_conf.length], tf.int64) for feature_conf in
                      features_conf}

    def tfrecord_parser(value, is_pred):
        # example_schema = {}
        feature_data = {field_name: field_tensor for field_name, field_tensor in
                        tf.parse_example(value, features=feature_schema).items()}
        if is_pred:
            return feature_data
        else:
            label_data = {field_name: field_tensor for field_name, field_tensor in
                          tf.parse_example(value, features=label_schema).items()}
            # feature_data = {field_name: tf.cast(field_tensor, tf.int32) for field_name, field_tensor in tf.parse_example(value, features=feature_schema).items()}
            # label_data = {field_name: tf.cast(field_tensor, tf.int32) for field_name, field_tensor in tf.parse_example(value, features=label_schema).items()}
            return feature_data, label_data

    return tfrecord_parser


def input_fn(files, data_type, batch_size, tmp_path, is_pred, decode_fn, num_epochs=None, num_preprocess_threads=2):
    """Reads input data num_epochs times.
    """
    with tf.name_scope('input'):
        if 'tfrecord' in data_type:
            dataset = tf.data.Dataset.from_tensor_slices(files).interleave(
                map_func=lambda x: tf.data.TFRecordDataset(x, compression_type="GZIP"),
                cycle_length=num_preprocess_threads, block_length=math.ceil(batch_size / num_preprocess_threads),
                num_parallel_calls=num_preprocess_threads
                )
            # dataset = tf.data.Dataset.from_tensor_slices(files).apply(
            #     tf.data.experimental.parallel_interleave(
            #         lambda filename: tf.data.TFRecordDataset(filename, compression_type="GZIP"),
            #         cycle_length=num_preprocess_threads, sloppy=True, prefetch_input_elements=batch_size))
            # dataset = tf.data.TFRecordDataset(files, compression_type="GZIP")#, num_parallel_reads=num_preprocess_threads)
        else:
            dataset = tf.data.Dataset.from_tensor_slices(files).interleave(
                lambda x: tf.data.TextLineDataset(x).prefetch(10), cycle_length=num_preprocess_threads)
        # dataset = dataset.cache(os.path.join(tmp_path, "data"))
        if not is_pred:
            dataset = dataset.shuffle(buffer_size=batch_size * 10)
        if num_epochs is None:
            dataset = dataset.repeat()
        elif num_epochs > 1:
            dataset = dataset.repeat(num_epochs)
        dataset = dataset.apply(tf.data.experimental.ignore_errors())

        dataset = dataset.batch(batch_size,
                                drop_remainder=True)  # dataset.apply(tf.contrib.data.batch_and_drop_remainder(batch_size))
        dataset = dataset.map(lambda x: decode_fn(x, is_pred))  # , num_parallel_calls=tf.data.experimental.AUTOTUNE)
        dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)
        return dataset
