import tensorflow as tf
import os
from tensorflow.python.client import timeline
from file.wfile import wfile
class GraphHandler(object):

    """GraphHandler: TensorFlow session session management and graph correlation"""

    def __init__(self, model,session,ckpt_path):
        self.model = model
        self.session=session
        self.ckpt_path=ckpt_path
        #self.saver = tf.train.Saver(max_to_keep=1)
        self.writer = None

    def get_session(self, sess):
        session = sess
        while type(session).__name__ != 'Session':
            # pylint: disable=W0212
            session = session._sess
        return session

    def destroy(self):
        """Terminates session and destroyes graph."""
        self.session.close()
        self.model.graph = None

class PerformRecoder(object):

    """save and update mechanism of checkpoint"""

    def __init__(self,saver,top_limit_num=1):
        self.top_limit_num = top_limit_num
        self.dev_top_list = []  # list of tuple(step, dev_accu)
        self.saver = saver

    def update_top_list(self, global_step, dev_accu, session,graph_handler,ckpt_path,data_round,checkpoint_dir):
        cur_ckpt_path = self.ckpt_file_path_generator(checkpoint_dir,data_round,global_step)
        self.dev_top_list.append([global_step, dev_accu])
        self.dev_top_list = list(sorted(self.dev_top_list, key=lambda elem: elem[1], reverse=True))

        print ('auc length:',len(self.dev_top_list))
        if len(self.dev_top_list) <= self.top_limit_num:
            self.create_ckpt_file(session,graph_handler,checkpoint_dir,data_round,global_step)
            return True, None
        elif len(self.dev_top_list) == self.top_limit_num + 1:
            out_state = self.dev_top_list[-1]
            self.dev_top_list = self.dev_top_list[:-1]
            if out_state[0] == global_step:
                return False, None
            else:  # add and delete
                print ('delete out_state:',out_state[0])
                self.delete_ckpt_file(self.ckpt_file_path_generator(checkpoint_dir,data_round,out_state[0]))
                self.create_ckpt_file(session,graph_handler,checkpoint_dir,data_round,global_step)
                return True, out_state[0]

        else:
            raise RuntimeError()

    def ckpt_file_path_generator(self,ckpt_path,i_epoch,step):
        return os.path.join(ckpt_path, 'epoch_%d-%d' % (i_epoch,step))

    def delete_ckpt_file(self, ckpt_file_path):
        print ('delete ckpt_file_path is:',ckpt_file_path)
        if os.path.isfile(ckpt_file_path+'.meta'):
            wfile.Remove(ckpt_file_path+'.meta')
        if os.path.isfile(ckpt_file_path+'.index'):
            wfile.Remove(ckpt_file_path+'.index')
        if os.path.isfile(ckpt_file_path+'.data-00000-of-00001'):
            wfile.Remove(ckpt_file_path+'.data-00000-of-00001')

    def create_ckpt_file(self, session,graph_handler,checkpoint_dir,i_epoch,global_step):
        print ('checkpoint save ckpt_path:',checkpoint_dir+'/epoch_' + str(i_epoch))
        self.saver.save(graph_handler.get_session(session),checkpoint_dir+'/epoch_' + str(i_epoch), global_step=global_step)

def timeline_profile(model_profiler,steps,profile_path,run_metadata,train_writer):

    "Model time performance evaluation function"

    model_profiler.add_step(step=steps, run_meta=run_metadata)
    opts_builder = (tf.profiler.ProfileOptionBuilder(
        tf.profiler.ProfileOptionBuilder.time_and_memory())
                    .order_by("bytes")
                    .with_step(steps))
    op1 = opts_builder.with_file_output(
        outfile=profile_path + "/profile-graph-%s.log" % steps).build()
    model_profiler.profile_graph(options=op1)

    op2 = opts_builder.with_file_output(
        outfile=profile_path + "/profile-op-%s.log" % steps).build()
    model_profiler.profile_graph(options=op2)

    op3 = opts_builder.with_file_output(
        outfile=profile_path + "/profile-nameandscope-%s.log" % steps).build()
    model_profiler.profile_name_scope(options=op3)

    train_writer.add_run_metadata(run_metadata, 'step-%03d' % steps)

    train_writer.close()
    # timeline
    trace = timeline.Timeline(step_stats=run_metadata.step_stats)
    time_path = profile_path + '/chrome-timeline-trace-' + str(steps)
    print("====Print timeline file :", os.path.abspath(time_path))
    timeline_writer = open(time_path, 'w')
    timeline_writer.write(trace.generate_chrome_trace_format())
    timeline_writer.close()