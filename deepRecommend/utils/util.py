import numpy as np
from sklearn.metrics import roc_auc_score
import heapq
import tensorflow as tf
import time
import os
import re
from collections import defaultdict
from file.wfile import wfile
import json


def get_evaluation(sess, model, dev_handle, num, mode):
    "Evaluate some training data sets and the number of incoming evaluations."

    def calc_auc(x, y):
        from sklearn import metrics
        fpr, tpr, thresholds = metrics.roc_curve(y, x)
        auc_result = metrics.auc(fpr, tpr)
        return auc_result

    # print('getting evaluation result for %s' % data_type)

    logits_list, wide_logits_list, deep_logits_list = [], [], []
    wide_loss_list, deep_loss_list = [], []
    loss_list = []
    auc_predict_list = []
    auc_wide_predict_list = []
    auc_deep_predict_list = []
    auc_label_list = []
    for _ in range(1, int(num) + 1):
        loss, logits, logits_wide, logits_deep, labels = sess.run(
            [model.loss, model.logits, model.logits_wide, model.logits_deep, model.labels['click_label']],
            feed_dict={model.handle: dev_handle,
                       model.is_train: False})  # The assessment and prediction parameters are false

        logits_list.append(np.argmax(logits, -1))
        wide_logits_list.append(np.argmax(logits_wide, -1))
        deep_logits_list.append(np.argmax(logits_deep, -1))
        loss_list.append(loss)
        # wide_loss_list.append(wide_loss)
        # deep_loss_list.append(deep_loss)
        auc_predict_list.append(logits)
        auc_wide_predict_list.append(logits_wide)
        auc_deep_predict_list.append(logits_deep)
        auc_label_list.append(np.array(labels).astype(float).tolist())

    # wide_loss_value = np.mean(wide_loss_list)
    # deep_loss_value = np.mean(deep_loss_list)
    loss_value = np.mean(loss_list)
    auc_predict = np.concatenate(auc_predict_list, 0)
    auc_wide_predict = np.concatenate(auc_wide_predict_list, 0)
    auc_deep_predict = np.concatenate(auc_deep_predict_list, 0)
    auc_label = np.concatenate(auc_label_list, 0)
    auc_value = calc_auc(auc_predict, auc_label)
    wide_auc_value = calc_auc(auc_wide_predict, auc_label)
    deep_auc_value = calc_auc(auc_deep_predict, auc_label)

    print('~~> Evaluation result for %s -- loss: %.4f -- auc: %.4f, wide_auc: %.4f, deep_auc: %.4f -- len: %d'
          % (mode, loss_value, auc_value, wide_auc_value, deep_auc_value, len(auc_predict_list)))
    return auc_value


def get_evaluation_all_rw(sess, model, validation_handle, i_epoch, data_type, avg_rdtm_size, global_step=None):
    "Calculate the AUC of the entire dataset"
    timer = Timer()
    print('Evaluation result: %s' % data_type)
    steps = 0
    # validation
    wide_loss_save = []
    deep_loss_save = []
    logits_save = []
    wide_logits_save = []
    deep_logits_save = []
    label_save = []
    tag_save = []
    rw_save = []
    pdt_save = []
    # Data initialization is required.
    if data_type == 'validation':
        sess.run(model.validation_iterator.initializer)
    else:
        sess.run(model.test_iterator.initializer)
    while True:
        try:
            ops_to_val = [model.wide_loss, model.deep_loss, model.logits, model.logits_wide, model.logits_deep,
                          model.label, model.tag, model.reweight, model.pdt]
            result_val = sess.run(ops_to_val, feed_dict={model.handle: validation_handle,
                                                         model.is_train: False})  # The assessment and prediction parameters are false
            wide_loss_val, deep_loss_val, logits, logits_wide, logits_deep, label, tag, rw, pdt = result_val
            wide_loss_save.append(wide_loss_val)
            deep_loss_save.append(deep_loss_val)
            logits_save.append(logits)
            wide_logits_save.append(logits_wide)
            deep_logits_save.append(logits_deep)
            label_save.append(label)
            tag_save.append(tag)
            rw_save.append(rw)
            pdt_save.append(pdt)
            steps += 1

        except tf.errors.OutOfRangeError:
            pred_list = np.concatenate(logits_save).reshape(-1)
            wide_pred_list = np.concatenate(wide_logits_save).reshape(-1)
            deep_pred_list = np.concatenate(deep_logits_save).reshape(-1)
            label_list = np.concatenate(label_save).reshape(-1)
            tag_list = np.concatenate(tag_save).reshape(-1)
            tag_set = set(tag_list)
            rw_list = np.concatenate(rw_save).reshape(-1)
            pdt_list = np.concatenate(pdt_save).reshape(-1)
            # 分场景计算auc
            for tag in tag_set:
                tag_auc_predict = []
                tag_auc_label = []
                for x, y, z in zip(tag_list, pred_list, label_list):
                    if x == tag:
                        tag_auc_predict.append(float(y))
                        tag_auc_label.append(int(z))
                auc_value = roc_auc_score(tag_auc_label, tag_auc_predict)
                print('Evaluation result:%d -- all steps:%d, tag:%s, duration:%.4f -- auc: %.4f' %
                      (i_epoch, steps, tag, timer.elapsed(), auc_value))
            # auc计算
            auc_cal = roc_auc_score(label_list, pred_list)
            wide_auc_cal = roc_auc_score(label_list, wide_pred_list)
            deep_auc_cal = roc_auc_score(label_list, deep_pred_list)
            # loss
            wide_loss = np.mean(wide_loss_save)
            deep_loss = np.mean(deep_loss_save)
            # AVG_RDTM
            # 取出正样本的idx
            pos_label_idx = np.where(label_list > 0)
            # 取出正样本对应值
            pos_pred = pred_list[pos_label_idx]
            pos_rw = rw_list[pos_label_idx]
            pos_pdt = pdt_list[pos_label_idx]
            # 取出正样本n个最大的预测分的index
            positive_sample_size = len(pos_pred)
            maxn_pos_pred_idx = heapq.nlargest(int(avg_rdtm_size * positive_sample_size), range(len(pos_pred)),
                                               pos_pred.take)
            maxn_pos_rw = pos_rw[maxn_pos_pred_idx]
            maxn_pos_pdt = pos_pdt[maxn_pos_pred_idx]
            avg_wrdtm = np.mean(maxn_pos_rw)
            avg_rdtm = np.mean(maxn_pos_pdt)

            print('Evaluation result:%d -- all steps:%d, duration:%.4f -- wide_loss:%.4f, deep_loss:%.4f' %
                  (i_epoch, steps, timer.elapsed(), wide_loss, deep_loss))
            print(
                'Evaluation result:%d -- auc:%.4f, wide_auc:%.4f, deep_auc:%.4f -- positive sample size:%d, eval rdtm size:%d, avg_wrdtm:%.4f, avg_rdtm:%.4f' %
                (i_epoch, auc_cal, wide_auc_cal, deep_auc_cal, positive_sample_size, len(maxn_pos_rw), avg_wrdtm,
                 avg_rdtm))
            break

    return auc_cal


def tensorflow_save_parameters_with_partition(sess, save_path, out_type='txt', tmp_dir='/tmp/dl_rank'):
    tmp_dir = os.path.join(tmp_dir, "out")
    if not wfile.Exists(tmp_dir): wfile.MakeDirs(tmp_dir)
    if save_path.startswith('s3'):
        import boto3
        if wfile.Exists(tmp_dir):
            wfile.DeleteRecursively(tmp_dir)
        wfile.MakeDirs(tmp_dir)
        s3 = boto3.resource('s3')
    save_path = os.path.join(save_path, out_type)
    if wfile.Exists(save_path):
        wfile.DeleteRecursively(save_path)
    variables = sorted(sess.graph.get_collection(tf.GraphKeys.VARIABLES), key=lambda var: var.name)
    variables_names = [var.name for var in variables]
    var_dict = defaultdict(list)
    for idx, var_name in enumerate(variables_names):
        # var_name, tail = var_name.rsplit('/', 1)
        if '/' in var_name and re.match('part_\d+:\d+', var_name.rsplit('/', 1)[1]):
            save_name = var_name.rsplit('/', 1)[0]
            part = int(var_name.rsplit('/', 1)[1].split('_')[1].split(':')[0])
            var_dict[save_name].append((part, variables[idx]))
        else:
            save_name = var_name.split(':')[0]
            var_dict[save_name].append((0, variables[idx]))
    for name, tensors in var_dict.items():
        file_name = os.path.join(save_path, name)
        file_path = file_name.rsplit('/', 1)[0]
        if not wfile.Exists(file_path):
            wfile.MakeDirs(file_path)
        merge_tensor = sess.run(list(map(lambda x: x[1], sorted(tensors, key=lambda x: x[0]))))
        if len(merge_tensor) > 1:
            min_row_num = min([part.shape[0] for part in merge_tensor])
            normal_row = np.concatenate([np.expand_dims(part[:min_row_num], 1) for part in merge_tensor], axis=1)
            res_row = [part[min_row_num:] for part in merge_tensor]
            # normal_row = itertools.chain.from_iterable(zip(*[list(row) for row in normal_row]))
            merge_tensor = np.concatenate([normal_row.reshape(-1, *normal_row.shape[2:]), *res_row], axis=0)
        else:
            merge_tensor = merge_tensor[0]
            if merge_tensor.ndim == 0:
                merge_tensor = np.expand_dims(merge_tensor, 0)
        if save_path.startswith('s3'):
            local_path = os.path.join(tmp_dir, name)
            _, _, bucketname, emr_path = file_name.split('/', 3)
            if not wfile.Exists(local_path.rsplit('/', 1)[0]):
                wfile.MakeDirs(local_path.rsplit('/', 1)[0])
            if out_type == 'npy':
                np.save(local_path, merge_tensor)
                s3.Bucket(bucketname).upload_file(local_path, emr_path)
            else:
                np.savetxt(local_path, merge_tensor)
                s3.Bucket(bucketname).upload_file(local_path, emr_path)
            pass
        else:
            if out_type == 'npy':
                np.save(file_name, merge_tensor)
            else:
                np.savetxt(file_name, merge_tensor)
        print("{} weight has been export".format(name))
        del merge_tensor


def _tensorflow_save_parameters_with_partition(ckpt_reader, VARIABLES_LIST, save_path, out_type='txt'):
    if save_path.startswith('s3'):
        import boto3
        if wfile.Exists('/tmp/dl_rank'):
            wfile.DeleteRecursively('/tmp/dl_rank')
        wfile.MakeDirs('/tmp/dl_rank')
        s3 = boto3.resource('s3')
    save_path = os.path.join(save_path, out_type)
    if wfile.Exists(save_path):
        wfile.DeleteRecursively(save_path)
    variables_names = [var_name.split(':')[0] for var_name in sorted(VARIABLES_LIST, key=lambda var: var)]
    var_dict = defaultdict(list)
    for idx, var_name in enumerate(variables_names):
        # var_name, tail = var_name.rsplit('/', 1)
        if '/' in var_name and re.match('part_\d+$', var_name.rsplit('/', 1)[1]):
            save_name = var_name.rsplit('/', 1)[0]
            var_dict[save_name] = [save_name]
        else:
            var_dict[var_name].append(var_name)
    for name, tensors in var_dict.items():
        file_name = os.path.join(save_path, name)
        file_path = file_name.rsplit('/', 1)[0]
        if not wfile.Exists(file_path):
            wfile.MakeDirs(file_path)
        merge_tensor = [ckpt_reader.get_tensor(tensor) for tensor in tensors]
        if len(merge_tensor) > 1:
            min_row_num = min([part.shape[0] for part in merge_tensor])
            normal_row = np.concatenate([np.expand_dims(part[:min_row_num], 1) for part in merge_tensor], axis=1)
            res_row = [part[min_row_num:] for part in merge_tensor]
            # normal_row = itertools.chain.from_iterable(zip(*[list(row) for row in normal_row]))
            merge_tensor = np.concatenate([normal_row.reshape(-1, *normal_row.shape[2:]), *res_row], axis=0)
        else:
            merge_tensor = merge_tensor[0]
            if merge_tensor.ndim == 0:
                merge_tensor = np.expand_dims(merge_tensor, 0)
        if save_path.startswith('s3'):
            local_path = os.path.join('/tmp/dl_rank', name)
            _, _, bucketname, emr_path = file_name.split('/', 3)
            if not wfile.Exists(local_path.rsplit('/', 1)[0]):
                wfile.MakeDirs(local_path.rsplit('/', 1)[0])
            if out_type == 'npy':
                np.save(local_path + '.npy', merge_tensor)
                s3.Bucket(bucketname).upload_file(local_path + '.npy', emr_path + '.npy')
            else:
                np.savetxt(local_path + '.txt', merge_tensor)
                s3.Bucket(bucketname).upload_file(local_path + '.txt', emr_path + '.txt')
            pass
        else:
            if out_type == 'npy':
                np.save(file_name, merge_tensor)
            else:
                np.savetxt(file_name, merge_tensor)
        del merge_tensor


class Timer():
    def __init__(self):
        self.start_time = time.time()

    def elapsed(self):
        end_time = time.time()
        duration = end_time - self.start_time
        self.start_time = end_time
        return duration


def normalize(x):
    if " " in x:
        return " ".join([str(float(w)) for w in x.split(" ")])
    else:
        return str(float(x))


def assemble_bucketInfo(name, value, bucket_info):
    if bucket_info['bucketType'].lower() == "quantile":
        BUCKET_QUANTILE_TEMPLETE['name'] = name
        BUCKET_QUANTILE_TEMPLETE['quantiles'] = bucket_info['bucketInfo']
        BUCKET_QUANTILE_TEMPLETE['bucketCount'] = bucket_info['size']
        BUCKET_QUANTILE_TEMPLETE['bucket'] = value
        return json.dumps(BUCKET_QUANTILE_TEMPLETE)
    elif bucket_info['bucketType'].lower() == "min_max":
        BUCKET_MIN_MAX_TEMPLETE['name'] = name
        BUCKET_MIN_MAX_TEMPLETE['min'] = bucket_info['minValue']
        BUCKET_MIN_MAX_TEMPLETE['max'] = bucket_info['maxValue']
        BUCKET_MIN_MAX_TEMPLETE['bucketCount'] = bucket_info['size']
        BUCKET_MIN_MAX_TEMPLETE['bucket'] = value
        return json.dumps(BUCKET_MIN_MAX_TEMPLETE)
    else:
        assert False, "can't find bucket type parser: {}".format(bucket_info['bucketType'])


BUCKET_MIN_MAX_TEMPLETE = {"protocol": "even", "name": "", "min": 0.0, "max": 1.0, "bucketCount": 3000, "bucket": []}
BUCKET_QUANTILE_TEMPLETE = {"protocol": "quantile", "name": "", "quantiles": [], "bucketCount": 3000, "bucket": []}
