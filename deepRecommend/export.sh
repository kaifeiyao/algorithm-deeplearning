#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin

date=$1
data_version=$2
experiment_version=$3
partition=$4


#date="2019-12-21"

project_dir=$(cd `dirname $0` && pwd)
conf_path="s3://jiayun-recommend/private/wangqi/wide_deep/data/${date}/${data_version}/Feature/profile.json"

function export(){
    cd ${project_dir}

    nohup python3 -u main.py \
       --mode export \
       --version ${experiment_version} \
       --partition ${partition} \
       --conf_path ${conf_path} \
       --date ${date} > export_task.log &
}

export
