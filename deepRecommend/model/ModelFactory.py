from .model_base import BaseModel
# from .wide_and_deep import wdl
# from .xcross import xcross
# from .xdeepfm import xdeepfm
# from .dcn import dcn
# from .deepfm_v2 import deepfm as deepfm_v2
# from .deepfm import deepfm
from .wide import wide
from .wide_deep import wide_deep_traditional_attention
from .deep import deep_traditional_attention
import os
import importlib.util

model_dict = {"wide_deep_traditional_attention": wide_deep_traditional_attention,
              "deep_traditional_attention": deep_traditional_attention,
              "wide": wide}


# model_dict = {'wdl': wdl, 'xcross': xcross, 'dcn': dcn, 'xdeepfm': xdeepfm, 'deepfm': deepfm,
#               'deepfm_v2': deepfm_v2, 'wide_and_deep_traditional_attention': wide_and_deep_traditional_attention}

def build(model_type, mode, model_conf, feature_conf, external_path=''):
    if model_type not in model_dict:
        exter_module_path = os.path.join(external_path, model_type + '.py')
        if os.path.exists(exter_module_path):
            exter_spec = importlib.util.spec_from_file_location('dl_rank.model.' + model_type, exter_module_path)
            exter_module = importlib.util.module_from_spec(exter_spec)
            exter_spec.loader.exec_module(exter_module)
            _find_submodel(exter_module)
        else:
            assert False, 'Cant find external model: {}'.format(exter_module_path)
    model = model_dict[model_type](mode, model_conf, feature_conf)
    return model


def register(model):
    name = model.name if hasattr(model, 'name') else model.__name__
    if name in model_dict:
        print('name: {} has been used'.format(name))
    else:
        model_dict.update({name: model})


def _find_submodel(module):
    for attr in dir(module):
        attrObj = getattr(module, attr)
        try:
            if not isinstance(attrObj, baseModel) and issubclass(attrObj, baseModel):
                register(attrObj)
        except:
            pass
