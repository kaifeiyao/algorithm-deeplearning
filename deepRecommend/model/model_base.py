import os
from abc import ABCMeta, abstractmethod

import tensorflow as tf

from file.wfile import wfile
from utils.dataset import input_fn, parse_fn


class BaseModel(metaclass=ABCMeta):
    partition = 10

    def __init__(self, mode, model_conf, feature_conf):
        self.mode = mode
        self.model_conf = model_conf
        self.feature_conf = feature_conf
        BaseModel.partition = model_conf.partition
        self.graph = None
        self.is_train = None
        self.label_name = []

    def build_embedding_params(self):
        """
        build embedding features from features info
        :return: None
        """

        embedding = dict()
        for feature in self.feature_conf:
            with tf.variable_scope(feature.variable_scope, reuse=tf.AUTO_REUSE):
                embedding.update({feature.name: tf.get_variable(
                    initializer=eval(feature.initializer['name'])(**feature.initializer['param']),
                    shape=[feature.size, feature.dim],
                    partitioner=tf.fixed_size_partitioner(BaseModel.partition),
                    name='{}_embedding'.format(feature.variable_name))})
        return embedding

    def build_input(self, data_conf, run_conf):

        """
        The training and prediction processes are separated by mode to support training, evaluation and prediction.
        The training mode requires 3 kinds of data by default, and can be evaluated according to the parameters.
        When the task is online prediction, the default lookup table operation is used, and the prediction input is the build network task.
        """
        data_decode_function = parse_fn(data_conf, self.feature_conf)
        self.label_name = data_conf.labels
        if self.mode == 'train':

            self.test_dataset = input_fn(data_conf.eval_data, data_conf.data_type,
                                         batch_size=self.model_conf.batch_size, tmp_path=run_conf.tmp_path,
                                         is_pred=False,
                                         decode_fn=data_decode_function, num_epochs=run_conf.max_epoch,
                                         num_preprocess_threads=run_conf.nthreads)
            self.handle = tf.placeholder(tf.string, shape=[])
            self.test_iterator = self.test_dataset.make_initializable_iterator()

            if self.mode == 'train':
                self.training_dataset = self.build_train_input(data_conf,
                                                               run_conf)  # .prefetch(buffer_size=self.model_conf.batch_size)
                self.validation_dataset = self.build_eval_input(data_conf, run_conf)

                self.training_iterator = self.training_dataset.make_initializable_iterator()
                self.validation_iterator = self.validation_dataset.make_initializable_iterator()
                iterator = tf.data.Iterator.from_string_handle(self.handle, self.training_dataset.output_types,
                                                               self.training_dataset.output_shapes)
            else:
                iterator = tf.data.Iterator.from_string_handle(self.handle, self.test_dataset.output_types,
                                                               self.test_dataset.output_shapes)
            return iterator.get_next()
        if self.mode == 'export':
            return self.build_export_input(data_conf, run_conf)

    @abstractmethod
    def build_export_input(self, data_conf=None, run_conf=None, num_epochs=None):
        pass

    def build_train_input(self, data_conf, run_conf, num_epochs=1):
        data_decode_function = parse_fn(data_conf, self.feature_conf)
        tmp_path = os.path.join(run_conf.tmp_path, "train")
        if wfile.Exists(tmp_path): wfile.DeleteRecursively(tmp_path)
        wfile.MakeDirs(tmp_path)
        return input_fn(data_conf.train_data, data_conf.data_type,
                        batch_size=self.model_conf.batch_size, tmp_path=tmp_path, is_pred=False,
                        decode_fn=data_decode_function, num_epochs=num_epochs,
                        num_preprocess_threads=run_conf.nthreads)

    def build_eval_input(self, data_conf, run_conf, num_epochs=None):
        data_decode_function = parse_fn(data_conf, self.feature_conf)
        tmp_path = os.path.join(run_conf.tmp_path, "eval")
        if wfile.Exists(tmp_path): wfile.DeleteRecursively(tmp_path)
        wfile.MakeDirs(tmp_path)
        return input_fn(data_conf.eval_data, data_conf.data_type,
                        batch_size=self.model_conf.batch_size, tmp_path=tmp_path, is_pred=False,
                        decode_fn=data_decode_function, num_epochs=num_epochs,
                        num_preprocess_threads=run_conf.nthreads)

    def build_test_input(self, data_conf, run_conf, num_epochs=1):
        data_decode_function = parse_fn(data_conf, self.feature_conf)
        tmp_path = os.path.join(run_conf.tmp_path, "test")
        if wfile.Exists(tmp_path): wfile.DeleteRecursively(tmp_path)
        wfile.MakeDirs(tmp_path)
        return input_fn(data_conf.eval_data, data_conf.data_type,
                        batch_size=self.model_conf.batch_size, tmp_path=tmp_path, is_pred=True,
                        decode_fn=data_decode_function, num_epochs=num_epochs,
                        num_preprocess_threads=run_conf.nthreads)

    @abstractmethod
    def build_network(self):
        pass

    @abstractmethod
    def build_optimize(self):
        pass

    @abstractmethod
    def build_loss(self):
        pass

    def build_debugger(self):

        "Verify the situation of Nan"
        # tf.add_to_collection( 'Asserts',
        #                       tf.verify_tensor_all_finite( self.loss, msg= "loss has nan or inf", name='assert_loss_contains_no_nan' )
        #                       )
        tf.add_to_collection('Asserts',
                             tf.verify_tensor_all_finite(self.logits, msg="outputs has nan or inf",
                                                         name='assert_outputs_contains_no_nan')
                             )
        self.assert_op = tf.group(*tf.get_collection('Asserts'))

    def build_graph(self, features, labels, data_conf, run_conf):

        """
        :param : Network structure flow chart
        :return: create a session
        """
        self.features_input, self.labels = features, labels
        self.graph.seed = 7777  # self.seed
        self.global_step = tf.train.get_or_create_global_step()
        self._embedding_parameters = self.build_embedding_params()
        self.build_network()

    def build_train_op(self, run_conf, sync_replica=None):
        # if self.mode != 'predict':
        self.loss = self.build_loss()
        self.build_debugger()
        # self.build_count()
        self.summary = tf.summary.merge_all()

        self.optimizer = self.build_optimize()

        if self.model_conf.optimizer.lower() in ['combine', 'ftrl_adadelta', 'ftrl_adam']:
            self.trainer = []
            self.wide_optimizer, self.deep_optimizer = self.optimizer[0], self.optimizer[1]
            wide_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "wide_layer")
            print('trainable wide var num: %d' % len(wide_vars))
            deep_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "deep_layer")
            print('trainable deep var num: %d' % len(deep_vars))

            with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
                self.trainer.append(self.wide_optimizer.minimize(self.loss, self.global_step, var_list=wide_vars))
                self.trainer.append(
                    self.deep_optimizer.minimize(self.loss, self.global_step, var_list=deep_vars))
        else:
            if run_conf.distribute['sync_train']:
                self.optimizer = tf.train.SyncReplicasOptimizer(self.optimizer,
                                                                replicas_to_aggregate=sync_replica,
                                                                total_num_replicas=sync_replica)

            with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
                self.trainer = self.optimizer.minimize(self.loss, global_step=self.global_step)

        self.init_all_vars = tf.group(tf.global_variables_initializer(), tf.initialize_local_variables())

        # self.saver = tf.train.Saver()

    @abstractmethod
    def get_eval_metric_ops(self, is_train, field):
        pass
