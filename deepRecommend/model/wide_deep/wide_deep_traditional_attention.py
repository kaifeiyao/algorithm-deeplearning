# from .BaseModel import baseModel, get_initializer
from functools import reduce
from operator import mul

import numpy as np
import tensorflow as tf

from layers import layer
from model.model_base import BaseModel
from model.wide_deep.wide_deep import wide_and_deep

VERY_BIG_NUMBER = 1e30
VERY_SMALL_NUMBER = 1e-30
VERY_POSITIVE_NUMBER = VERY_BIG_NUMBER
VERY_NEGATIVE_NUMBER = -VERY_BIG_NUMBER


# wide columns

class wide_deep_traditional_attention(wide_and_deep):
    name = 'wide_deep_traditional_attention'

    def __init__(self, mode, model_conf, feature_conf):
        super(wide_and_deep, self).__init__(mode, model_conf, feature_conf)

    def build_network(self):

        """Wide_layer is a basic LR model, including click order, transaction order,
        item statistical characteristics and query pair pairs. The deep model connects
        to two traditional attention models, and finally adds the output layer linearly.
        """

        with tf.variable_scope("wide_layer_variable", reuse=tf.AUTO_REUSE) as scope:
            self.b = tf.get_variable(name='bias', shape=[], trainable=True)
        with tf.variable_scope("wide_layer") as scope:
            if self.mode == 'train':
                wide_feature_input = []
                wide_feature_conf = [f for f in self.feature_conf if 'wide' in f.group]
                for f_conf in wide_feature_conf:
                    u_emb = self.features_input[f_conf.name]
                    indicies = tf.where(tf.not_equal(u_emb, -1))
                    w_sp = tf.SparseTensor(values=tf.gather_nd(u_emb, indicies), indices=indicies,
                                           dense_shape=[self.model_conf.batch_size, f_conf.length])
                    feature_sum = tf.nn.embedding_lookup_sparse(self._embedding_parameters[f_conf.name], w_sp, None,
                                                                combiner='sum', name="SparseSum")
                    feature_sum_fill_tail = tf.pad(feature_sum,
                                                   [[0, self.model_conf.batch_size - tf.shape(feature_sum)[0]], [0, 0]],
                                                   'CONSTANT', constant_values=0)
                    wide_feature_input.append(feature_sum_fill_tail)
                add_sum = tf.reduce_sum(wide_feature_input, axis=0)
                self.add_sum = tf.reshape(add_sum, [-1], name='wide_sum')

                self.logits_wide = self.add_sum + self.b
                self.ctr = tf.sigmoid(self.logits_wide)

                print('logits_wide_shape', self.logits_wide.get_shape())

        with tf.variable_scope("deep_layer", reuse=tf.AUTO_REUSE) as scope:
            sequence_feature_conf = [f for f in self.feature_conf if 'sequence' in f.group][0]
            item_feature_conf = [f for f in self.feature_conf if 'itemid' in f.group][0]
            if self.mode == 'train':
                sequence_emb = layer.embedding_lookup(self._embedding_parameters[sequence_feature_conf.name],
                                                      self.features_input[sequence_feature_conf.name], 0)
                itemid_emb = layer.embedding_lookup(self._embedding_parameters[item_feature_conf.name],
                                                    self.features_input[item_feature_conf.name], 0)
                sequence_token_mask = tf.cast(
                    tf.slice(self.features_input[sequence_feature_conf.name], [0, 0], [self.model_conf.batch_size,
                                                                                       sequence_feature_conf.length]),
                    tf.bool)
                itemid_token_mask = tf.cast(self.features_input[item_feature_conf.name], tf.bool)  ###
                self.logits_deep = self.bulid_attention_layers(sequence_emb, sequence_token_mask, itemid_emb,
                                                               itemid_token_mask, 'attention')
                # tp2 = tf.nn.embedding_lookup(self._embedding_parameters[sequence_feature_conf.name],
                #                              [[0], [1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12], [13], [14], [15]])
                # tp = tf.get_default_graph().get_tensor_by_name('deep_layer_variable/itemid_embedding/part_0:0')
                # tfprint = tf.print(tp2, summarize=1000)
                # tfprint1 = tf.print("pid", self.features_input[item_feature_conf.name], itemid_emb, summarize=1000)
                # tfprint2 = tf.print('click', self.features_input[sequence_feature_conf.name], sequence_emb, summarize=1000)
                # with tf.control_dependencies([tfprint]):
                #     self.logits_deep = tf.identity(logits_deep)
            else:
                # deep input
                self.sequence = self.features_input['sequence']
                self.itemid = self.features_input['itemid']
                sequence_expand = tf.expand_dims(tf.reduce_mean(self.sequence, 1), 0)
                sequence_token_mask = tf.cast(sequence_expand, tf.bool)
                self.bs = tf.shape(self.itemid)[0]
                print('bs_shape', self.bs)
                itemid_token_mask = tf.cast(tf.reduce_mean(tf.expand_dims(self.itemid, 1), 2), tf.bool)
                self.logits_deep = self.bulid_attention_layers(tf.expand_dims(self.sequence, 0), sequence_token_mask,
                                                               tf.expand_dims(self.itemid, 1),
                                                               itemid_token_mask, 'attention')
            print('logits_deep_shape', self.logits_deep.get_shape())

        with tf.variable_scope("output_layer"):
            if self.mode == 'train':
                # tfprint = tf.print(self.logits_deep, summarize=10000)
                # with tf.control_dependencies([tfprint]):
                self.logits = self.logits_wide + self.logits_deep
                tf.summary.histogram('logits_wide', self.logits_wide)
                tf.summary.histogram('logits_deep', self.logits_deep)
                tf.summary.histogram('logits', self.logits)
            else:
                self.logits = self.logits_deep
                # print('logits_shape', self.logits.get_shape())
                self.y = tf.identity(self.logits_deep, name='predict_value')

        return self.logits

    def bulid_attention_layers(self, u_emb, all_token_mask, itemid_emb, temid_token_mask, scope):
        with tf.variable_scope('attention', partitioner=tf.fixed_size_partitioner(BaseModel.partition)):
            if not self.mode == 'train':
                sequence_rep_single = single_attention(
                    u_emb, all_token_mask, scope + '_sequence', self.model_conf.dropout, self.is_train,
                    self.model_conf.wd,
                    'elu', None, 's1_attention', is_multi_att=False, attention_dim=None
                )
                sequence_rep = tf.tile(sequence_rep_single, multiples=[self.bs, 1])
                tf.get_variable_scope().reuse_variables()
                itemid_rep = single_attention(
                    itemid_emb, temid_token_mask, scope + '_sequence', self.model_conf.dropout, self.is_train,
                    self.model_conf.wd,
                    'elu', None, 's2_attention', is_multi_att=False, attention_dim=None
                )
            else:
                sequence_rep = single_attention(
                    u_emb, all_token_mask, scope + '_sequence', self.model_conf.dropout, self.is_train,
                    self.model_conf.wd,
                    'elu', None, 's1_attention', is_multi_att=False, attention_dim=None
                )
                tf.get_variable_scope().reuse_variables()
                itemid_rep = single_attention(
                    itemid_emb, temid_token_mask, scope + '_sequence', self.model_conf.dropout, self.is_train,
                    self.model_conf.wd,
                    'elu', None, 's2_attention', is_multi_att=False, attention_dim=None
                )
            out_rep = tf.concat([sequence_rep, itemid_rep, sequence_rep - itemid_rep, sequence_rep * itemid_rep],
                                1)  # [bs,128*4]
        with tf.variable_scope('output_layer_last', partitioner=tf.fixed_size_partitioner(BaseModel.partition)):
            attention_layer = dice(
                linear([out_rep], self.model_conf.hidden_num, True, 0., scope=scope + '_output_attention_layer',
                       squeeze=False,
                       wd=self.model_conf.wd, input_keep_prob=self.model_conf.dropout, is_train=self.is_train),
                name='attention_pre_out')

            # logits_deep_dropout = tf.nn.dropout(attention_layer,self.cfg.last_dropout)
            # logits_deep = tf.layers.dense(inputs=logits_deep_dropout, units=1, activation=None, use_bias=True)[:,0]
            logits_deep = linear([attention_layer], 1, True, 0., scope=scope + '_logits', squeeze=False,
                                 wd=self.model_conf.wd, input_keep_prob=self.model_conf.last_dropout,
                                 is_train=self.is_train)[:, 0]

        # tfprint = tf.print('u_emb', out_rep, summarize=1000)
        # with tf.control_dependencies([tfprint]):
        #     logits_deep = tf.identity(logits_deep)
        return logits_deep

    def build_loss(self):

        """Using sigmoid_cross_entropy_with_logits, the shape constraints of label and Logits are consistent and one-dimensional array."""
        labels = tf.cast(self.labels[self.label_name[0]], tf.float32, name='true_label')
        labels = tf.slice(labels, [0], tf.shape(self.logits))
        # wide loss
        wide_losses = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=self.logits_wide)
        # self.wide_loss_sum = tf.reduce_sum(wide_losses, name='wide_loss_sum')
        wide_loss = tf.reduce_mean(wide_losses)
        # tf.summary.scalar('wide_loss_sum', self.wide_loss_sum)
        tf.summary.scalar('wide_loss', wide_loss)
        # deep loss
        deep_losses = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=self.logits_deep)
        # self.deep_loss_sum = tf.reduce_sum(deep_losses, name='deep_loss_sum')
        deep_loss = tf.reduce_mean(deep_losses)
        # tf.summary.scalar('deep_loss_sum', self.deep_loss_sum)
        tf.summary.scalar('deep_loss', deep_loss)
        all_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=self.logits),
                                  name="all_loss")
        return all_loss

    def get_evaluation(self, sess, dev_handle, num, mode):
        "Evaluate some training data sets and the number of incoming evaluations."

        def calc_auc(x, y):
            from sklearn import metrics
            fpr, tpr, thresholds = metrics.roc_curve(y, x)
            auc_result = metrics.auc(fpr, tpr)
            return auc_result

        # print('getting evaluation result for %s' % data_type)

        logits_list, wide_logits_list, deep_logits_list = [], [], []
        wide_loss_list, deep_loss_list = [], []
        loss_list = []
        auc_predict_list = []
        auc_wide_predict_list = []
        auc_deep_predict_list = []
        auc_label_list = []
        for _ in range(1, int(num) + 1):
            loss, logits, logits_wide, logits_deep, labels = sess.run(
                [self.loss, self.logits, self.logits_wide, self.logits_deep, self.labels[self.label_name[0]]],
                feed_dict={self.handle: dev_handle,
                           self.is_train: False})  # The assessment and prediction parameters are false

            logits_list.append(np.argmax(logits, -1))
            wide_logits_list.append(np.argmax(logits_wide, -1))
            deep_logits_list.append(np.argmax(logits_deep, -1))
            loss_list.append(loss)
            # wide_loss_list.append(wide_loss)
            # deep_loss_list.append(deep_loss)
            auc_predict_list.append(logits)
            auc_wide_predict_list.append(logits_wide)
            auc_deep_predict_list.append(logits_deep)
            auc_label_list.append(np.array(labels).astype(float).tolist())

        # wide_loss_value = np.mean(wide_loss_list)
        # deep_loss_value = np.mean(deep_loss_list)
        loss_value = np.mean(loss_list)
        auc_predict = np.concatenate(auc_predict_list, 0)
        auc_wide_predict = np.concatenate(auc_wide_predict_list, 0)
        auc_deep_predict = np.concatenate(auc_deep_predict_list, 0)
        auc_label = np.concatenate(auc_label_list, 0)
        auc_value = calc_auc(auc_predict, auc_label)
        wide_auc_value = calc_auc(auc_wide_predict, auc_label)
        deep_auc_value = calc_auc(auc_deep_predict, auc_label)

        print('~~> Evaluation result for %s -- loss: %.4f -- auc: %.4f, wide_auc: %.4f, deep_auc: %.4f -- len: %d'
              % (mode, loss_value, auc_value, wide_auc_value, deep_auc_value, len(auc_predict_list)))
        return auc_value


def single_attention(rep_tensor, rep_mask, scope=None,
                     keep_prob=1., is_train=None, wd=0., activation='elu',
                     tensor_dict=None, name='', is_multi_att=False, attention_dim=None):
    """single attention: contain two traditional attention struct.

    Args:
      rep_tensor: list tensor,shape is [batch_size,list_size,embedding_dim].
      rep_mask: whether the marker Tensor is 0,bool value, shape is [batch_size,list_size]
    Returns:
      Attention representation of tensor,shape is [batch_size,embedding_dim]
    Raises:
      TypeError: If the input dimension is incorrect.
    """

    with tf.variable_scope(scope):
        attention_rep_first_layer = traditional_attention(
            rep_tensor, rep_mask, 'traditional_attention',
            keep_prob, is_train, wd, activation,
            tensor_dict=tensor_dict, name=name + '_attn')

        return attention_rep_first_layer


def get_logits(args, size, bias, bias_start=0.0, scope=None, mask=None, wd=0.0,
               input_keep_prob=1.0, is_train=None, func=None):
    if func is None:
        func = "linear"
    if func == 'sum':
        return sum_logits(args, mask=mask, name=scope)
    elif func == 'linear':
        return linear_logits(args, bias, bias_start=bias_start, scope=scope, mask=mask, wd=wd,
                             input_keep_prob=input_keep_prob,
                             is_train=is_train)
    elif func == 'dot':
        assert len(args) == 2
        arg = args[0] * args[1]
        return sum_logits([arg], mask=mask, name=scope)
    elif func == 'mul_linear':
        assert len(args) == 2
        arg = args[0] * args[1]
        return linear_logits([arg], bias, bias_start=bias_start, scope=scope, mask=mask, wd=wd,
                             input_keep_prob=input_keep_prob,
                             is_train=is_train)
    elif func == 'proj':
        assert len(args) == 2
        d = args[1].get_shape()[-1]
        proj = linear([args[0]], d, False, bias_start=bias_start, scope=scope, wd=wd, input_keep_prob=input_keep_prob,
                      is_train=is_train)
        return sum_logits([proj * args[1]], mask=mask)
    elif func == 'tri_linear':
        assert len(args) == 2
        new_arg = args[0] * args[1]
        return linear_logits([args[0], args[1], new_arg], bias, bias_start=bias_start, scope=scope, mask=mask, wd=wd,
                             input_keep_prob=input_keep_prob,
                             is_train=is_train)
    else:
        raise Exception()


def dice(_x, axis=-1, epsilon=0.0000001, name=''):
    # _x: [batch_size, 10]
    alphas = tf.get_variable('alpha' + name, _x.get_shape()[-1],
                             initializer=tf.constant_initializer(0.0),
                             dtype=tf.float32)

    input_shape = list(_x.get_shape())
    reduction_axes = list(range(len(input_shape)))

    del reduction_axes[axis]  # [0]

    broadcast_shape = [1] * len(input_shape)  # [1,1]
    broadcast_shape[axis] = input_shape[axis]  # [1 * hidden_unit_size]

    # case: train mode (uses stats of the current batch)
    mean = tf.reduce_mean(_x, axis=reduction_axes)  # [1 * hidden_unit_size]
    brodcast_mean = tf.reshape(mean, broadcast_shape)
    std = tf.reduce_mean(tf.square(_x - brodcast_mean) + epsilon, axis=reduction_axes)
    std = tf.sqrt(std)
    brodcast_std = tf.reshape(std, broadcast_shape)  # [1 * hidden_unit_size]
    # x_normed = (_x - brodcast_mean) / (brodcast_std + epsilon)
    x_normed = tf.layers.batch_normalization(_x, center=False, scale=False)  # a simple way to use BN to calculate x_p
    x_p = tf.sigmoid(x_normed)

    return alphas * (1.0 - x_p) * _x + x_p * _x


def traditional_attention(rep_tensor, rep_mask, scope=None,
                          keep_prob=1., is_train=None, wd=0., activation='elu',
                          tensor_dict=None, name=None, output_dim=None):
    bs, sl, vec = tf.shape(rep_tensor)[0], tf.shape(rep_tensor)[1], tf.shape(rep_tensor)[2]
    if output_dim is None:
        ivec = rep_tensor.get_shape()[2]
    else:
        ivec = output_dim
    with tf.variable_scope(scope):
        # TODO: TEST: batch_normal enable
        rep_tensor_map = bn_dense_layer(rep_tensor, ivec, True, 0., 'bn_dense_map', activation,
                                        True, wd, keep_prob, is_train)
        rep_tensor_logits = get_logits([rep_tensor_map], None, False, scope='self_attn_logits',
                                       mask=rep_mask, input_keep_prob=keep_prob, is_train=is_train)  # bs,sl
        attn_res = softsel(rep_tensor, rep_tensor_logits, rep_mask)  # bs,vec

        # save attn
        # if tensor_dict is not None and name is not None:
        #    tensor_dict[name] = tf.nn.softmax(rep_tensor_logits)

        return attn_res


def bn_dense_layer(input_tensor, hn, bias, bias_start=0.0, scope=None,
                   activation='relu', enable_bn=True,
                   wd=0., keep_prob=1.0, is_train=None):
    if is_train is None:
        is_train = False

    # activation
    if activation == 'linear':
        activation_func = tf.identity
    elif activation == 'relu':
        activation_func = tf.nn.relu
    elif activation == 'elu':
        activation_func = tf.nn.elu
    elif activation == 'selu':
        activation_func = selu
    else:
        raise AttributeError('no activation function named as %s' % activation)

    with tf.variable_scope(scope or 'bn_dense_layer'):
        linear_map = linear(input_tensor, hn, bias, bias_start, 'linear_map',
                            False, wd, keep_prob, is_train)
        if enable_bn:
            linear_map = tf.layers.batch_normalization(linear_map, center=True, scale=True, training=is_train)
            # linear_map = tf.contrib.layers.batch_norm(
            #     linear_map, center=True, scale=True, is_training=is_train, scope='bn')
        return activation_func(linear_map)


def softsel(target, logits, mask=None, scope=None):
    """
    :param target: [ ..., J, d] dtype=float #(b,sn,sl,ql,d)
    :param logits: [ ..., J], dtype=float
    :param mask: [ ..., J], dtype=bool
    :param scope:
    :return: [..., d], dtype=float
    """
    with tf.name_scope(scope or "Softsel"):
        a = softmax(logits, mask=mask)
        target_rank = len(target.get_shape().as_list())
        out = tf.reduce_sum(tf.expand_dims(a, -1) * target, target_rank - 2)
        return out


def sum_logits(args, mask=None, name=None):
    with tf.name_scope(name or "sum_logits"):
        if args is None or (isinstance(args, (tuple, list)) and not args):
            raise ValueError("`_args` must be specified")
        if not isinstance(args, (tuple, list)):
            args = [args]
        rank = len(args[0].get_shape())
        logits = sum(tf.reduce_sum(arg, rank - 1) for arg in args)
        if mask is not None:
            logits = exp_mask(logits, mask)
        return logits


def softmax(logits, mask=None, scope=None):
    with tf.name_scope(scope or "Softmax"):
        if mask is not None:
            logits = exp_mask(logits, mask)
        out = tf.nn.softmax(logits, -1)
        return out


def linear(args, output_size, bias, bias_start=0.0, scope=None, squeeze=False, wd=0.0, input_keep_prob=1.0,
           is_train=None):
    if args is None or (isinstance(args, (tuple, list)) and not args):
        raise ValueError("`_args` must be specified")
    if not isinstance(args, (tuple, list)):
        args = [args]

    flat_args = [flatten(arg, 1) for arg in args]  # for dense layer [(-1, d)]
    if input_keep_prob < 1.0:
        assert is_train is not None
        # TODO: attention if need update when u have time
        flat_args = [tf.layers.dropout(arg, rate=input_keep_prob, training=is_train) for arg in flat_args]
        # flat_args = [tf.cond(is_train, lambda: tf.nn.dropout(arg, input_keep_prob), lambda: arg)# for dense layer [(-1, d)]
        #              for arg in flat_args]
    flat_out = _linear(flat_args, output_size, bias, bias_start=bias_start, scope=scope)  # dense
    out = reconstruct(flat_out, args[0], 1)  # ()
    if squeeze:
        out = tf.squeeze(out, [len(args[0].get_shape().as_list()) - 1])

    if wd:
        add_reg_without_bias()
    return out


def flatten(tensor, keep):
    fixed_shape = tensor.get_shape().as_list()
    start = len(fixed_shape) - keep
    left = reduce(mul, [fixed_shape[i] or tf.shape(tensor)[i] for i in range(start)])
    out_shape = [left] + [fixed_shape[i] or tf.shape(tensor)[i] for i in range(start, len(fixed_shape))]
    flat = tf.reshape(tensor, out_shape)
    return flat


def _linear(xs, output_size, bias, bias_start=0., scope=None):
    with tf.variable_scope(scope or 'linear_layer'):
        x = tf.concat(xs, -1)
        input_size = x.get_shape()[-1]
        W = tf.get_variable('W', shape=[input_size, output_size], dtype=tf.float32, )
        if bias:
            bias = tf.get_variable('bias', shape=[output_size], dtype=tf.float32,
                                   initializer=tf.constant_initializer(bias_start))
            out = tf.matmul(x, W) + bias
        else:
            out = tf.matmul(x, W)
        return out


def add_reg_without_bias(scope=None):
    pass
    # scope = scope or tf.get_variable_scope().name
    # variables = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope=scope)
    # counter = 0
    # for var in variables:
    #     if len(var.get_shape().as_list()) <= 1: continue
    #     tf.add_to_collection('reg_vars', var)
    #     counter += 1
    # return counter


def reconstruct(tensor, ref, keep, dim_reduced_keep=None):
    dim_reduced_keep = dim_reduced_keep or keep

    ref_shape = ref.get_shape().as_list()  # original shape
    tensor_shape = tensor.get_shape().as_list()  # current shape
    ref_stop = len(ref_shape) - keep  # flatten dims list
    tensor_start = len(tensor_shape) - dim_reduced_keep  # start
    pre_shape = [ref_shape[i] or tf.shape(ref)[i] for i in range(ref_stop)]  #
    keep_shape = [tensor_shape[i] or tf.shape(tensor)[i] for i in range(tensor_start, len(tensor_shape))]  #
    # pre_shape = [tf.shape(ref)[i] for i in range(len(ref.get_shape().as_list()[:-keep]))]
    # keep_shape = tensor.get_shape().as_list()[-keep:]
    target_shape = pre_shape + keep_shape
    out = tf.reshape(tensor, target_shape)
    return out


def selu(x):
    with tf.name_scope('elu') as scope:
        alpha = 1.6732632423543772848170429916717
        scale = 1.0507009873554804934193349852946
        return scale * tf.where(x >= 0.0, x, alpha * tf.nn.elu(x))


def linear_logits(args, bias, bias_start=0.0, scope=None, mask=None, wd=0.0, input_keep_prob=1.0, is_train=None):
    with tf.variable_scope(scope or "Linear_Logits"):
        logits = linear(args, 1, bias, bias_start=bias_start, squeeze=True, scope='first',
                        wd=wd, input_keep_prob=input_keep_prob, is_train=is_train)
        if mask is not None:
            logits = exp_mask(logits, mask)
        return logits


def exp_mask(val, mask, name=None):
    """Give very negative number to unmasked elements in val.
    For example, [-3, -2, 10], [True, True, False] -> [-3, -2, -1e9].
    Typically, this effectively masks in exponential space (e.g. softmax)
    Args:
        val: values to be masked
        mask: masking boolean tensor, same shape as tensor
        name: name for output tensor

    Returns:
        Same shape as val, where some elements are very small (exponentially zero)
    """
    if name is None:
        name = "exp_mask"
    return tf.add(val, (1 - tf.cast(mask, 'float')) * VERY_NEGATIVE_NUMBER, name=name)
