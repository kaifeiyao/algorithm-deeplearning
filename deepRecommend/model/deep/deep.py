import tensorflow as tf
from abc import ABCMeta, abstractmethod
from utils.dataset import input_fn, parse_fn
from file.wfile import wfile
from model.model_base import BaseModel
import os
from tensorflow.python.framework import graph_util
from file.wfile import wfile
from utils.util import normalize, assemble_bucketInfo
import numpy as np


class Deep(BaseModel):
    partition = 10

    def __init__(self, mode, model_conf, feature_conf):
        super(Deep, self).__init__(mode, model_conf, feature_conf)

    def build_embedding_params(self):
        """
        build embedding features from features info
        :return: None
        """

        embedding = dict()
        for feature in self.feature_conf:
            if 'wide' not in feature.group:
                with tf.variable_scope(feature.variable_scope, reuse=tf.AUTO_REUSE):
                    embedding.update({feature.name: tf.get_variable(
                        initializer=eval(feature.initializer['name'])(**feature.initializer['param']),
                        shape=[feature.size, feature.dim],
                        partitioner=tf.fixed_size_partitioner(BaseModel.partition),
                        name='{}_embedding'.format(feature.variable_name))})
        return embedding

    def union_info_weight(self, id_path, online_graph_dir, base_bucket_info, variable_path, model_path, out_path):

        if not wfile.Exists(os.path.join(out_path, "data")):
            wfile.MakeDirs(os.path.join(out_path, "data"))
        # deep_id   REMIND: remove first weight value, because it's belong to default
        deep_weight_path, feature_name = [(os.path.join(variable_path, "txt", feature_info.variable_scope,
                                                        feature_info.variable_name) + "_embedding",
                                           feature_info.variable_name) for feature_info in self.feature_conf if
                                          feature_info.group != 'wide'][0]
        deep_out_path = os.path.join(out_path, "data", "deep")
        key = wfile.Open(id_path[feature_name], 'r')
        value = wfile.Open(deep_weight_path, 'r')
        out = wfile.Open(deep_out_path, 'w')

        row_number = 0
        _default = value.readline()
        idx = key.readline().strip()
        weight = value.readline().strip()
        while idx != '' and weight != '' and row_number < 50000000:
            normal_weight = normalize(weight)
            out.write(idx + '|' + normal_weight + '\n')
            row_number += 1
            idx = key.readline().strip()
            weight = value.readline().strip()
        key.close()
        value.close()
        out.close()

    @abstractmethod
    def build_network(self):
        pass

    def build_optimize(self):
        learning_rate = tf.train.exponential_decay(self.model_conf.learning_rate, self.global_step,
                                                   self.model_conf.lr_decay_steps, self.model_conf.lr_decay_rate,
                                                   staircase=True)
        # ---------- optimization ---------
        if self.model_conf.optimizer == 'adadelta':
            opt = tf.train.AdadeltaOptimizer(learning_rate, use_locking=True)
        elif self.model_conf.optimizer == 'adagrad':
            opt = tf.train.AdagradDAOptimizer(learning_rate=learning_rate,
                                              global_step=self.global_step,
                                              l1_regularization_strength=self.model_conf.l1 / self.model_conf.batch_size,
                                              l2_regularization_strength=self.model_conf.l2 / self.model_conf.batch_size,
                                              use_locking=True)

        elif self.model_conf.optimizer == 'sgd':
            opt = tf.train.GradientDescentOptimizer(learning_rate, use_locking=True)
        elif self.model_conf.optimizer == 'adam':
            opt = tf.contrib.opt.LazyAdamOptimizer(learning_rate=learning_rate, beta1=0.9,
                                                   beta2=0.999)
        elif self.model_conf.optimizer == 'rmsprop':
            opt = tf.train.RMSPropOptimizer(self.model_conf.learning_rate)
        elif self.model_conf.optimizer == 'ftrl':
            opt = tf.train.FtrlOptimizer(learning_rate=self.model_conf.learning_rate,
                                         initial_accumulator_value=1.0 / pow(self.model_conf.batch_size, 2),
                                         l1_regularization_strength=self.model_conf.l1 / self.model_conf.batch_size,
                                         l2_regularization_strength=self.model_conf.l2 / self.model_conf.batch_size,
                                         use_locking=True)

        else:
            raise AttributeError('no optimizer named as \'%s\'' % self.model_conf.optimizer)
        return opt

    def get_evaluation(self, sess, dev_handle, num, mode):
        "Evaluate some training data sets and the number of incoming evaluations."

        def calc_auc(x, y):
            from sklearn import metrics
            fpr, tpr, thresholds = metrics.roc_curve(y, x)
            auc_result = metrics.auc(fpr, tpr)
            return auc_result

        # print('getting evaluation result for %s' % data_type)

        logits_list, wide_logits_list, deep_logits_list = [], [], []
        wide_loss_list, deep_loss_list = [], []
        loss_list = []
        auc_predict_list = []
        auc_deep_predict_list = []
        auc_label_list = []
        for _ in range(1, int(num) + 1):
            loss, logits, logits_deep, labels = sess.run(
                [self.loss, self.logits, self.logits_deep, self.labels[self.label_name[0]]],
                feed_dict={self.handle: dev_handle,
                           self.is_train: False})  # The assessment and prediction parameters are false

            logits_list.append(np.argmax(logits, -1))
            deep_logits_list.append(np.argmax(logits_deep, -1))
            loss_list.append(loss)
            # wide_loss_list.append(wide_loss)
            # deep_loss_list.append(deep_loss)
            auc_predict_list.append(logits)
            auc_deep_predict_list.append(logits_deep)
            auc_label_list.append(np.array(labels).astype(float).tolist())

        # wide_loss_value = np.mean(wide_loss_list)
        # deep_loss_value = np.mean(deep_loss_list)
        loss_value = np.mean(loss_list)
        auc_predict = np.concatenate(auc_predict_list, 0)
        auc_deep_predict = np.concatenate(auc_deep_predict_list, 0)
        auc_label = np.concatenate(auc_label_list, 0)
        auc_value = calc_auc(auc_predict, auc_label)
        deep_auc_value = calc_auc(auc_deep_predict, auc_label)

        print('~~> Evaluation result for %s -- loss: %.4f -- auc: %.4f, deep_auc: %.4f -- len: %d'
              % (mode, loss_value, auc_value, deep_auc_value, len(auc_predict_list)))
        return auc_value

    def build_loss(self):

        """Using sigmoid_cross_entropy_with_logits, the shape constraints of label and Logits are consistent and one-dimensional array."""

        # trainable_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES,'deep_layer')
        # print('trainable var num: %d' % len(trainable_vars))
        labels = tf.cast(self.labels[self.label_name[0]], tf.float32, name='true_label')
        labels = tf.slice(labels, [0], tf.shape(self.logits))
        deep_l2_loss = 0.5 * tf.reduce_sum(
            [tf.nn.l2_loss(weight) for weight in tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "deep_layer/")])
        losses = tf.nn.sigmoid_cross_entropy_with_logits(labels=labels,
                                                         logits=self.logits)  # + self.model_conf.l2 / self.model_conf.batch_size * (deep_l2_loss)#+self.mba_loss)
        loss = tf.reduce_mean(losses)
        tf.summary.scalar('deep_loss', loss)

        # weights = tf.slice(self.reweight, [0], tf.shape(self.logits))
        self.loss_sum = tf.reduce_sum(losses, name='loss_sum')
        # self.weight_sum = tf.reduce_sum(weights, name='weight_sum')
        # tf.summary.scalar('loss_sum', self.loss_sum)
        # tf.summary.scalar('weight_sum', self.weight_sum)
        return loss

    def build_debugger(self):

        "Verify the situation of Nan"
        # tf.add_to_collection( 'Asserts',
        #                       tf.verify_tensor_all_finite( self.loss, msg= "loss has nan or inf", name='assert_loss_contains_no_nan' )
        #                       )
        tf.add_to_collection('Asserts',
                             tf.verify_tensor_all_finite(self.logits, msg="outputs has nan or inf",
                                                         name='assert_outputs_contains_no_nan')
                             )
        self.assert_op = tf.group(*tf.get_collection('Asserts'))

    def get_eval_metric_ops(self, is_train, field=""):
        pass
        """Return a dict of the evaluation Ops.
        Args:
            labels (Tensor): Labels tensor for training and evaluation.
            predictions (Tensor): Predictions Tensor.
        Returns:
            Dict of metric results keyed by name.
        """
        # labels = tf.cast(self.labels['click_label'], tf.float32, name='true_label')
        labels = tf.expand_dims(self.labels[self.label_name[0]], axis=1)

        deep_auc = tf.compat.v1.metrics.auc(
            labels=labels,
            predictions=tf.nn.sigmoid(self.logits_deep),
            name='auc',
            curve='ROC',
        )
        if is_train:
            tf.summary.scalar(field+"/deep_auc", deep_auc[1])
        return {
            'deep_auc': deep_auc
        }

    def build_export_input(self, data_conf=None, run_conf=None, shard_info=None):
        with tf.variable_scope("deep_layer", reuse=tf.AUTO_REUSE) as scope:
            sequence = tf.placeholder(tf.float32,
                                           [self.model_conf.reduce_sequence_length, self.model_conf.embedding_dim],
                                           name='sequence')
            itemid = tf.placeholder(tf.float32, [None, self.model_conf.embedding_dim], name='itemid')
        features = {'sequence': sequence, 'itemid': itemid}
        return features, None

    def build_graph(self, features, labels, data_conf, run_conf):

        """
        :param : Network structure flow chart
        :return: create a session
        """
        self.features_input, self.labels = features, labels
        self.global_step = tf.train.get_or_create_global_step()
        self._embedding_parameters = self.build_embedding_params()
        self.build_network()

    def build_train_op(self, run_conf, sync_replica=None):
        self.loss = self.build_loss()
        self.build_debugger()
        # self.build_count()
        self.summary = tf.summary.merge_all()

        self.optimizer = self.build_optimize()

        # if self.model_conf.optimizer.lower() in ['combine', 'ftrl_adadelta', 'ftrl_adam']:
        #     self.trainer = []
        #     self.wide_optimizer, self.deep_optimizer = self.optimizer[0], self.optimizer[1]
        #     wide_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "wide_layer")
        #     print('trainable wide var num: %d' % len(wide_vars))
        #     deep_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "deep_layer")
        #     print('trainable deep var num: %d' % len(deep_vars))
        #
        #     with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
        #         self.trainer.append(
        #             self.deep_optimizer.minimize(self.loss, self.global_step, var_list=deep_vars))
        # else:
        if run_conf.distribute['sync_train']:
            self.optimizer = tf.train.SyncReplicasOptimizer(self.optimizer,
                                                            replicas_to_aggregate=sync_replica,
                                                            total_num_replicas=sync_replica)

        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            self.trainer = self.optimizer.minimize(self.loss, global_step=self.global_step)

        self.init_all_vars = tf.group(tf.global_variables_initializer(), tf.initialize_local_variables())

