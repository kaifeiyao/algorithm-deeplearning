import tensorflow as tf


def embedding_lookup(params, ids, empty_value):
    ids_reshape = tf.reshape(ids, [tf.shape(ids)[0]*tf.shape(ids)[1], 1])
    ids_reshape_sparse = to_sparse(ids_reshape, [tf.shape(ids_reshape)[0], 1], empty_value)
    features_f_emb_reshape = tf.nn.embedding_lookup_sparse(params, ids_reshape_sparse, None, combiner="sum")
    features_f_emb_reshape = tf.pad(features_f_emb_reshape, [[0, tf.shape(ids_reshape)[0] - tf.shape(features_f_emb_reshape)[0]], [0, 0]],
                                                   'CONSTANT', constant_values=0)
    features_f_emb = tf.reshape(features_f_emb_reshape, [tf.shape(ids)[0], ids.shape.as_list()[1], features_f_emb_reshape.shape.as_list()[-1]])
    return features_f_emb


def to_sparse(dense_tensor, dense_shape, empty_value=0):
    indices = tf.where(tf.not_equal(dense_tensor, empty_value))
    values = tf.gather_nd(dense_tensor, indices)
    sparse = tf.SparseTensor(indices, values, dense_shape)
    return sparse
