import os

import tensorflow as tf

from TFEstimator import Estimator, Environment
from conf.config_container import ConfigContainer
from model import ModelFactory
from file.wfile import wfile
tf.compat.v1.disable_eager_execution()

config = ConfigContainer()


class TaskManager(object):
    def __init__(self, config, tf_env):
        self.conf = config
        self.env = tf_env

    def infer(self):
        pass

    def train(self):
        self.model = ModelFactory.build(self.conf.mission.model_name, "train", self.conf.model, self.conf.feature)
        self.estimator = Estimator(self.env.local_mode, self.env.job_name, self.model, self.conf.mission,
                                   self.conf.data)
        return self.estimator.train_and_eval()

    def export(self):
        if self.env.is_chief:
            os.environ.pop('TF_CONFIG', None)
            self.model = ModelFactory.build(self.conf.mission.model_name, "export", self.conf.model, self.conf.feature)
            self.estimator = Estimator(self.env.local_mode, self.env.job_name, self.model, self.conf.mission,
                                       self.conf.data)
            self.estimator.export_model(self.conf.data)
        return 0

    def test(self):
        self.model = ModelFactory.build(self.conf.mission.model_name, "test", self.conf.model, self.conf.feature)
        self.estimator = Estimator(self.env.local_mode, self.env.job_name, self.model, self.conf.mission,
                                   self.conf.data)
        return self.estimator.test()


    def export_custom(self):
        pass

    def run(self, mode):
        if mode == 'train':
            exit_code = self.train()
        elif mode == 'export' and self.env.is_chief:
            exit_code = self.export()
        elif mode == 'train_and_export':
            exit_code = self.train()
            if exit_code == 0 and self.env.is_chief:
                tf.reset_default_graph()
                self.export()
        elif mode == "test":
            self.test()
        else:
            print("mode: {} not exist".format(mode))
            exit_code = 1

        if self.env.is_chief and self.conf.mission.train_config.task_flag_dir != '':
            with wfile.Open(
                    self.conf.mission.train_config.task_flag_dir + "/" + self.conf.mission.train_config.version + '_' + self.conf.mission.train_config.date,
                    'w') as f:
                f.write(str(exit_code))
        os._exit(exit_code)


def main(cfg_container, env):
    cfg_container.init_environment()
    cfg_container.split_train_data(env.task_index + int(env.is_chief) * (env.worker_num - 1), env.worker_num)
    task = TaskManager(cfg_container, env)  # conf_dir=args.conf, log_dir=args.logpath, tmp_dir=args.logpath)
    task.run(cfg_container.mode)


if __name__ == "__main__":
    env = Environment(config.cluster_ips)
    if env.job_name == 'ps':
        pid = os.fork()
        if pid == 0:
            main(config, env)
        else:
            os._exit(0)
    else:
        main(config, env)
