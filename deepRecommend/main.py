import os
import tensorflow as tf
from Estimator import Estimator, Environment
from conf.config_container import ConfigContainer
from model import ModelFactory
from file.wfile import wfile
tf.compat.v1.disable_eager_execution()

config = ConfigContainer()
env = Environment(config.cluster_ips)

config.init_environment()
config.split_train_data(env.task_index + int(env.is_chief) * (env.worker_num - 1), env.worker_num)


class TaskManager(object):
    def __init__(self, config):
        self.conf = config

    def infer(self):
        pass

    @env.distribution
    def train(self):
        self.model = ModelFactory.build(self.conf.mission.model_name, "train", self.conf.model, self.conf.feature)
        self.estimator = Estimator(env.local_mode, True, self.model, self.conf.mission, self.conf.data)
        self.estimator.initialize_session(env.server, env.is_chief, self.conf.mission.run_config, env.task_index)
        return self.estimator.train_and_eval(env.task_index, env.worker_num, env.is_chief)

    def export(self):
        if env.is_chief:
            os.environ.pop('TF_CONFIG', None)
            self.model = ModelFactory.build(self.conf.mission.model_name, "export", self.conf.model, self.conf.feature)
            self.estimator = Estimator(True, False, self.model, self.conf.mission, self.conf.data)
            self.estimator.export_model(self.conf.data)
        return 0

    def export_custom(self):
        pass

    def run(self, mode):
        if mode == 'train':
            exit_code = self.train()
        elif mode == 'export' and env.is_chief:
            exit_code = self.export()
        elif mode == 'train_and_export':
            exit_code = self.train()
            if exit_code == 0 and env.is_chief:
                tf.reset_default_graph()
                self.export()
        else:
            print("mode: {} not exist".format(mode))
            exit_code = 1

        if env.is_chief and self.conf.mission.train_config.task_flag_dir != '':
            with wfile.Open(self.conf.mission.train_config.task_flag_dir + "/" + self.conf.mission.train_config.version + '_' + self.conf.mission.train_config.date, 'w') as f:
                f.write(str(exit_code))
        os._exit(exit_code)

if __name__ == "__main__":
    task = TaskManager(config)  # conf_dir=args.conf, log_dir=args.logpath, tmp_dir=args.logpath)
    task.run(config.mode)