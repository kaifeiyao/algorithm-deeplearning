#!/usr/bin/env bash

key_path=$1
date=$2
mode=$3
experiment_config=$4
experiment_version=$5
script_name=$6
ps_num=$7

#key_path=keyPath #"/home/hadoop/"${emrName}".pem"
project_dir=$(cd `dirname $0` && pwd)
run_path=${project_dir}"/run/"${script_name}".sh"

if [[ -z "$key_path" ]];then
    echo "Error, not define key_path!" > run.log
    exit 999
fi

if [[ -z "$date" ]];then
    echo "Error, not define experiment date!" > run.log
    exit 999
fi

if [[ -z "$mode" ]];then
    echo "Error, not define mode!" > run.log
    exit 999
fi

if [[ -z "$experiment_config" ]];then
    echo "Error, not define experiment_config!" > run.log
    exit 777
fi

if [[ -z "$experiment_version" ]];then
    echo "Error, not define experiment_version!" > run.log
    exit 777
fi

if [[ -z "$script_name" ]];then
    echo "Error, not define run_path!" > run.log
    exit 777
fi

if [[ -z "$ps_num" ]];then
    echo "Error, not define ps number!" > run.log
    exit 999
fi

local_ip=$(ifconfig | grep 'inet addr:172' | awk '{print $2}' | sed 's/addr://g')

function get_ip_list(){
    clusterId=$(cat /mnt/var/lib/info/job-flow.json | jq -r ".jobFlowId")
    ipList=$(aws emr list-instances --region us-west-2 --cluster-id ${clusterId} | jq '.Instances' | jq '.[]' -c | jq '.PrivateIpAddress' | tr -d '"')
    echo $(echo ${ipList} | tr ' ' ',')
}

cluster_ips=$(get_ip_list)
echo ${cluster_ips}


if [[ ${mode} == "train" || ${mode} == "train_and_export" || ${mode} == "export" ]]
then
    run_command="nohup bash ${run_path} ${date} ${mode} ${experiment_config} ${experiment_version} ${ps_num} ${cluster_ips} > run.log 2>&1 &"
    for ip in $(echo ${cluster_ips} | tr "," "\n");
    do
        if [[ $ip != $local_ip ]];then
            scp -i $key_path -o StrictHostKeyChecking=no -r $project_dir hadoop@${ip}:/home/hadoop > /dev/null
            ssh -i $key_path -o StrictHostKeyChecking=no hadoop@$ip "${run_command}"
        else
            eval ${run_command}
        fi
    done
fi
