from file.wfile import wfile
import os
import yaml


def load_conf(conf_path):

    def wrapper(conf_name):
        with wfile.Open(os.path.join(conf_path, conf_name + '.yaml'), 'r') as f:
            return yaml.load(f)
    return wrapper
'''
--graph_path /Users/snoworday/data/wide_deep/experiment/2019-02-17/graph --export_path /Users/snoworday/data/wide_deep/experiment/2019-02-17/export --log_path /Users/snoworday/data/wide_deep/experiment/2019-02-17/log_data --result_path /Users/snoworday/data/wide_deep/experiment/2019-02-17/result --online_graph_path /Users/snoworday/data/wide_deep/experiment/2019-02-17/result/online_graph/wide_and_deep_traditional_attention_v3/ --profile_path /Users/snoworday/data/wide_deep/experiment/2019-02-17/profile --tmp_path /tmp/dl_rank --conf_path /Users/snoworday/Library/Containers/com.apple.TextEdit/Data/Downloads/profile-2019-02-17.json --mode export
'''
