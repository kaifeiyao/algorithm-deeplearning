from .config_manager import ModelConfig, MissionConfig, DataConfig, FeatureConfig
import os
from file.wfile import wfile
from xml.etree.ElementTree import parse
from conf.config import cfg


class ConfigContainer(object):
    def __init__(self):
        self._config = cfg
        self.env = self._config.env
        self.hdfs_root = self._config.hdfs_root
        self.mode = cfg.mode
        self.cluster_ips = cfg.cluster_ips

    @property
    def mission(self):
        if not hasattr(self, '_mission'):
            self._mission = MissionConfig(self._config.mission)
        return self._mission

    @property
    def feature(self):
        if not hasattr(self, '_feature'):
            # _vocabulary = self.confKeeper['vocabulary']
            _feature = FeatureConfig(self._config.feature)
            _schema = self.data.schema
            self._feature = _feature.get_order_feature_dict(_schema)
        return self._feature

    @property
    def model(self):
        if not hasattr(self, '_model'):
            self._model = ModelConfig(self._config.model)
        return self._model

    @property
    def data(self):
        if not hasattr(self, '_data'):
            self._data = DataConfig(self._config.data)
            # train_part_num = self._data.split_data(self.tf_config['task_index'], len(self.tf_config['worker']))
            # print("part_num", train_part_num, "train_dataset", self._data.train_data)
            # self.mission.run_config.train_data_size /= train_part_num
        return self._data

    def split_train_data(self, index, num):
        train_part_num = self.data.split_data(index, num)
        print("part_num", train_part_num, "train_dataset_num", len(self.data.train_data))

    def init_environment(self):
        for key, value in self.env.items():
            os.environ[key] = value
        if self.hdfs_root != '':
            wfile.init_hdfs(self.hdfs_root)
        elif wfile.Exists('/etc/hadoop/conf/hdfs-site.xml'):
            with wfile.Open('/etc/hadoop/conf/hdfs-site.xml', 'r') as f:
                hdfs_doc = parse(f)
                for item in hdfs_doc.iterfind('property'):
                    if item.findtext('name') == 'dfs.namenode.rpc-address':
                        wfile.init_hdfs(item.findtext('value'))
                        break
