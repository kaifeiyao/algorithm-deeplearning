#!/usr/bin/env python
# encoding: utf-8
# Copyright 2018 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""config:input arguments """

import argparse
import json
import os
from datetime import datetime, timedelta
from xml.etree.ElementTree import parse
import multiprocessing

from file.wfile import wfile

# date = str(datetime.today() + timedelta(-1)).split(' ')[0]
DEBUG = False

back_param = {
    "a": {"lr": 0.01, "opt": "adam"},
    "b": {"lr": 0.1, "opt": "ftrl", "l1/l2": 0.1},
    "c": {"lr": 10, "opt": "sgd"},
    "d": {"opt": "adam", "lr": 0.1, "l2": 0.1}
}


def test(args):
    args.model = "wide_deep_traditional_attention"  # deep_traditional_attention"
    args.mode = "train"
    args.deep_learning_rate = 0.1
    args.l1 = 0.1
    args.l2 = 0.1
    args.partition = 6
    args.embedding_dim = 4
    args.batch_size = 16
    args.max_epoch = 5
    args.max_steps = 100000
    args.optimizer = 'adam'
    # args.parameter_path = "/Users/snoworday/data/wide_deep/experiment/2020-02-16/v3.json"
    args.learning_rate = 0.1
    args.eval_period = 60
    args.print_steps = 30
    args.tmp_path = '/tmp/dl_rank'
    args.experiment_path = "/Users/snoworday/data/wide_deep/experiment"
    args.export_path = "/Users/snoworday/data/wide_deep/export"
    args.train_data = "/Users/snoworday/data/wide_deep/data/2020-02-16/traindata"
    args.eval_data = "/Users/snoworday/data/wide_deep/data/2020-02-16/evaldata"
    args.graph_path = "/Users/snoworday/data/wide_deep/experiment/2020-02-16/graph"
    args.export_path = "/Users/snoworday/data/wide_deep/experiment/2020-02-16/export"
    args.log_path = "/Users/snoworday/data/wide_deep/experiment/2020-02-16/log_data"
    args.result_path = "/Users/snoworday/data/wide_deep/experiment/2020-02-16/result"
    args.online_graph_path = "/Users/snoworday/data/wide_deep/experiment/2020-02-16/online_graph"
    args.profile_path = "/Users/snoworday/data/wide_deep/experiment/2020-02-16Z/profile"
    args.conf_path = "/Users/snoworday/data/wide_deep/data/2020-02-16/Feature/2020-02-16.json"  # "/Users/snoworday/data/wide_deep/Feature/2019-12-17/profile.json"


class Configs(object):
    def __init__(self):
        # *************** parsing input arguments ***************
        parser = argparse.ArgumentParser()
        parser.register('type', 'bool', (lambda x: x.lower() in ("yes", "true", "t", "1")))
        # ------ task type ------------------
        parser.add_argument("--version", type=str, default="base", help="experiment version, default base")
        parser.add_argument("--mode", type=str, help='train|export')
        parser.add_argument("--num_ps", type=int, default=1)
        parser.add_argument("--local_ip", type=str, default='')
        parser.add_argument("--hdfs_root", type=str, default='')
        parser.add_argument("--cluster_ips", type=str, default='')
        parser.add_argument("--partition", type=int, default=12)
        parser.add_argument('--parameter_path', type=str, help='json file',
                            default='None')
        parser.add_argument('--conf_path', type=str, help='yaml file|json file',
                            default='s3://jiayun-recommend/private/wangqi/wide_deep/Feature/{}/profile.json')
        parser.add_argument('--disable_mkl', type=bool, default=False, help='use mkl-dnn in tf')

        # ----- Import Data Files ------------
        parser.add_argument("--date", type=str, default=str(datetime.today() + timedelta(-1)).split(' ')[0])
        parser.add_argument("--train_data", type=str,
                            default="s3://jiayun-recommend/private/wangqi/wide_deep/{}/traindata")
        parser.add_argument("--eval_data", type=str,
                            default="s3://jiayun-recommend/private/wangqi/wide_deep/{}/evaldata")

        parser.add_argument("--experiment_path", type=str, default="")
        parser.add_argument("--log_path", type=str, default=None)
        parser.add_argument("--profile_path", type=str, default=None)
        parser.add_argument("--graph_path", type=str, default=None)
        parser.add_argument("--online_graph_path", type=str, default=None)
        parser.add_argument("--result_path", type=str, default=None)
        parser.add_argument("--flag_path", type=str, default="")
        parser.add_argument("--export_path", type=str, default="s3://jiayun-recommend/common/deep_wide/pre")

        # ----- Import Running Control Parameters --------
        parser.add_argument("--random_seed", type=int, default=777)
        parser.add_argument("--tmp_path", type=str, default="/dev/shm/dl_rank")
        parser.add_argument('--sync_mode', type=int, default=0, help='distribute use sync_mode ?')
        parser.add_argument('--print_steps', type=int, default=400, help='strat print steps number')
        parser.add_argument('--eval_period', type=int, default=200000, help='evaluation period')
        parser.add_argument('--profile_period', type=int, default=100000, help='evaluation period')
        parser.add_argument('--validation_sample', type='bool', default=True, help='do not use')
        parser.add_argument('--train_eval_num', type=int, default=80, help='Train Eval Num')
        parser.add_argument('--log_period', type=int, default=2000, help='save tf summary period')
        parser.add_argument('--print_timeline', type='bool', default=False, help='do not use')
        parser.add_argument('--batch_size', type=int, default=2048, help='Test Batch Size')
        parser.add_argument('--train_data_size', type=int, default=233333, help='training dataset size')
        parser.add_argument('--max_steps', type=int, default=6000000, help='max steps number')
        parser.add_argument('--max_epoch', type=int, default=7, help='max epoch number')
        parser.add_argument('--num_threads', type=int, default=multiprocessing.cpu_count(), help='num threads')
        parser.add_argument("--model", type=str, default="wide_deep_traditional_attention")

        # ---- Import Model Control Parameters ---------
        parser.add_argument('--learning_rate', type=float, default=0.1, help='Init Wide Learning/all Learning')
        parser.add_argument('--lr_decay_steps', type=int, default=50000, help='Init Wide Learning/all Learning')
        parser.add_argument('--lr_decay_rate', type=float, default=0.96, help='Init Wide Learning/all Learning')
        parser.add_argument('--deep_learning_rate', type=float, default=0.2, help='Init Deep Learning rate')
        parser.add_argument('--l1', type=float, default=10.0, help='Learning rate decay')
        parser.add_argument('--l2', type=float, default=10.0, help='Learning rate decay')
        parser.add_argument('--optimizer', type=str, default='ftrl',
                            help='choose an optimizer[adadelta|adam|sgd]')
        parser.add_argument('--reduce_sequence_length', type=int, default=20, help='max sentence length')
        parser.add_argument('--dropout', type=float, default=0.75, help='dropout keep prob')
        parser.add_argument('--last_dropout', type=float, default=0.9, help='dropout keep prob')
        parser.add_argument('--hidden_num', type=int, default=64, help='Hidden units number of Neural Network')
        parser.add_argument('--wd', type=float, default=0, help='weight decay factor/l2 decay factor')
        parser.add_argument('--init_std', type=float, default=0.01, help='Learning rate decay')
        parser.add_argument('--embedding_dim', type=int, default=64, help='embedding length')

        # ------ Import Data Parser Parameters ---------
        parser.add_argument('--data_type', type=str, default='tfrecord')
        parser.add_argument('--labels', type=str, default='ctr_label')
        parser.add_argument('--schema', type=str,
                            default='cid,pid,click_seq,click_cross_seq,order_cross_seq,purchase_cross_seq,ctr,cvr,offlineSalesScore')
        parser.add_argument('--infer_data_format', type=str, default='cid,pid,features')
        parser.add_argument('--pred_out_delim', type=str, default=',')
        parser.add_argument('--primary_delim', type=str, default='@')
        parser.add_argument('--secondary_delim', type=str, default='|')
        parser.add_argument('--tertiary_delim', type=str, default=',')
        parser.add_argument('--train_data_format', type=str, default='ctr_label,features')

        # @ ---- args ----
        parser.set_defaults(shuffle=True)
        args = parser.parse_args()
        if DEBUG: test(args)

        self.hdfs_root = args.hdfs_root
        self.env = dict()
        # environ params initial
        # init_hdfs(args.hdfs_root)

        if args.disable_mkl:
            self.env['TF_DISABLE_MKL'] = '1'
        else:
            self.env['TF_DISABLE_MKL'] = '0'
            self.env['OMP_NUM_THREADS'] = str(args.num_threads)
            self.env['KMP_BLOCKTIME'] = '0'
            self.env['KMP_AFFINITY'] = 'granularity=fine,compact,1,0'  # test par

        if wfile.Exists(args.parameter_path):
            with wfile.Open(args.parameter_path, "r") as f:
                parameter = json.load(f)
                for param, value in parameter.items():
                    setattr(args, param, value)

        self.mode = args.mode

        cluster_ips = args.cluster_ips.split(',')
        # cluster_ips.sort()
        self.cluster_ips = {'cluster_ips': cluster_ips, 'num_ps': args.num_ps, 'local_ip': args.local_ip}
        if len(cluster_ips) > 1:
            num_ps = args.num_ps
            worker_num = len(cluster_ips) - num_ps
            task_index = cluster_ips.index(args.local_ip)
        else:
            num_ps = 1
            worker_num = 1
            task_index = 0

        args.partition = max(args.partition, num_ps)
        self.mission = {'runconfig': {}, 'train': {}}
        self.mission['runconfig'].update({
            "random_seed": args.random_seed,
            "tmp_path": args.tmp_path,
            "distribute": {"name": "PS", "sync_train": args.sync_mode, "sync_replica": task_index},
            "print_steps": args.print_steps,
            "eval_period": args.eval_period,
            'profile_period': args.profile_period,
            "validation_sample": args.validation_sample,
            "train_eval_num": args.train_eval_num,
            "log_period": args.log_period,
            "print_timeline": args.print_timeline,
            "batch_size": args.batch_size,
            "max_steps": args.max_steps,
            "max_epoch": args.max_epoch,
            "nthreads": args.num_threads,
            "fine_tune": False,
            "model_name": args.model
        })

        if args.experiment_path != '':
            args.log_path = os.path.join(args.experiment_path, "{}", "{}", "logData")
            args.graph_path = os.path.join(args.experiment_path, "{}", "{}", "graph")
            args.online_graph_path = os.path.join(args.experiment_path, "{}", "{}", "online_graph")
            args.profile_path = os.path.join(args.experiment_path, "{}", "{}", "profile")
            args.result_path = os.path.join(args.experiment_path, "{}", "{}", "result")
        self.mission['train'].update({
            'date': args.date,
            "version": args.version,
            'flag_dir': args.flag_path,
            'profile_dir': args.profile_path,
            'graph_dir': args.graph_path,
            'log_dir': args.log_path,
            'online_graph_dir': args.online_graph_path,
            'result_dir': args.result_path,
            'export_dir': os.path.join(args.export_path, args.version)
        })

        self.data = dict()
        self.data.update({
            'train_data': args.train_data,
            'eval_data': args.eval_data,
            'pred_data': '',
            'data_type': args.data_type,
            'labels': args.labels.split(','),
            'infer_data_format': '',
            'pred_out_delim': args.pred_out_delim,
            'primary_delim': args.primary_delim,
            'secondary_delim': args.secondary_delim,
            'tertiary_delim': args.tertiary_delim,
            'train_data_format': args.train_data_format.split(','),
            'schema': args.schema.split(',')
        })

        self.model = {
            'partition': args.partition,
            'learning_rate': args.learning_rate,
            'lr_decay_steps': args.lr_decay_steps,
            'lr_decay_rate': args.lr_decay_rate,
            'deep_learning_rate': args.deep_learning_rate,
            'l1': args.l1,
            'l2': args.l2,
            'batch_size': args.batch_size,
            'activation': 'relu',
            'optimizer': args.optimizer,
            'reduce_sequence_length': args.reduce_sequence_length,
            'init_std': args.init_std,
            'dropout': args.dropout,
            'last_dropout': args.last_dropout,
            'hidden_num': args.hidden_num,
            'wd': args.wd,
            'embedding_dim': args.embedding_dim
        }
        config_file = args.conf_path.format(args.date)

        self.feature = self.load_json(config_file, dataset_multi_factor=1.0 / worker_num,
                                      embedding_dim=args.embedding_dim)

    def load_json(self, filepath, **kwargs):
        """
        json file generate by 特征工程 task
        {"schema":[feature1, feature2, feature3], "labels:[label1, label2]", "train_data_size": Number, "feature": [group, dim, size, length, name]}
        :param filepath:
        :return:
        """
        feature_config = dict()
        with wfile.Open(filepath, 'r') as f:
            original_feature_conf = json.load(f)
            self.data['schema'] = [field for field in original_feature_conf['schema'] if
                                   field not in original_feature_conf['labels']]
            self.data['labels'] = original_feature_conf['labels']
            self.mission['train']['date'] = original_feature_conf['date']
            self.mission['runconfig']['train_data_size'] = int(
                original_feature_conf['train_data_size'] * kwargs['dataset_multi_factor'])
            self.data['train_data'] = original_feature_conf['trainData']
            self.data['eval_data'] = original_feature_conf['evalData']
            self.mission['train']['id_file_path'] = {id_name: info['path'] for id_name, info in original_feature_conf[
                'id'].items()}  # original_feature_conf['featurePath']

            self.data['bucket'] = original_feature_conf['bucket']
            self.data['id'] = original_feature_conf['id']

            used_bucket, used_id = set(), set()
            for feature_name, feature_info in original_feature_conf['feature'].items():
                if (not feature_name in original_feature_conf['labels']) and feature_name in self.data['schema']:
                    if feature_info['group'] == 'wide':
                        initializer = {"name": "tf.truncated_normal_initializer", "param": {"stddev": 0.01}}
                    else:
                        initializer = {"name": "tf.contrib.layers.xavier_initializer"}

                    if feature_info['type'] == 'id':
                        embedding = self.data['id'][feature_info['name']]['embedding']
                        zero = self.data['id'][feature_info['name']]['zero']
                        size = self.data['id'][feature_info['name']]['size']
                        used_id.add(feature_info['name'])
                    elif feature_info['type'] == 'bucket':
                        embedding = {'dim': 1, 'param': {'stddev': 0.01,
                                                         'mean': 0.0}}  # feature_info.get("embedding", {'dim': 1, 'param': {'stddev': 0.01, 'mean': 0.0}})
                        zero = -1
                        size = self.data['bucket'][feature_name]['size']
                        used_bucket.add(feature_name)
                    else:
                        assert False, "maybe numeric in future, not used now"

                    feature_config.update(
                        {feature_name: {
                            "group": feature_info['group'],
                            "embedding": embedding,
                            "dim": 1 if 'wide' in feature_info['group'] else kwargs['embedding_dim'],
                            # TODO, decide if set embedding_dim here is good choice #embedding["dim"],
                            "size": int(size) + int(zero) + 1,  # 需要将真实的size加1(默认值)，对于默认值是-1的不需要
                            "zero": int(zero),
                            "length": int(feature_info['length']),
                            "parameter": {
                                "name": feature_info['name'],
                                "scope": "wide_layer_variable" if feature_info[
                                                                      'group'] == 'wide' else "deep_layer_variable",
                                "initializer": initializer
                            }
                        }
                        }
                    )
            self.data['bucket'] = dict(filter(lambda bucket_feature: bucket_feature[0] in used_bucket, self.data['bucket'].items()))
            self.data['id'] = dict(filter(lambda id_feature: id_feature[0] in used_id, self.data['id'].items()))

        return feature_config

def init_hdfs(path):
    if path != '':
        wfile.init_hdfs(path)
    elif wfile.Exists('/etc/hadoop/conf/hdfs-site.xml'):
        with wfile.Open('/etc/hadoop/conf/hdfs-site.xml', 'r') as f:
            hdfs_doc = parse(f)
            for item in hdfs_doc.iterfind('property'):
                if item.findtext('name') == 'dfs.namenode.rpc-address':
                    wfile.init_hdfs(item.findtext('value'))
                    break


cfg = Configs()

##-----profile config example----
