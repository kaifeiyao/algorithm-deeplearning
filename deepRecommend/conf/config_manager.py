from file.wfile import wfile, pathStr
import json
import os


class FeatureConfig(object):

    def __init__(self, conf_dict):
        self.feature_conf = {name: FieldConfig(name, parameters) for name, parameters in conf_dict.items()}

    def get_order_feature_dict(self, schema):
        return [self.feature_conf[feature_name] for feature_name in schema]


class FieldConfig(object):

    def __init__(self, name, parameters):
        self.name = name
        self.group = parameters["group"]
        self.zero = parameters["zero"]
        self.size = parameters["size"]
        self.dim = parameters["dim"]
        self.length = parameters["length"]
        optional_parameters = parameters['parameter']
        self.variable_name = optional_parameters.pop("name") if "name" in optional_parameters else self.name
        self.variable_scope = optional_parameters.pop(
            "scope") if "scope" in optional_parameters else "embedding_layer_variable"

        initializer_name = optional_parameters['initializer'].get('name')
        initializer_param = optional_parameters['initializer'].get('param', dict())
        self.initializer = {'name': initializer_name, 'param': initializer_param}

    def __repr__(self):
        return json.dumps({'name': self.name, "group": self.group, "size": self.size, "dim": self.dim,
                           "length": self.length, "initializer": self.initializer['name']})


class DataConfig(object):

    def __init__(self, conf_dict):
        self.schema = conf_dict['schema']
        self.labels = conf_dict['labels']
        self.bucket_info = conf_dict['bucket']

        self.primary_delim = conf_dict.get('primary_delim', None)
        self.secondary_delim = conf_dict.get('secondary_delim', None)
        self.tertiary_delim = conf_dict.get('tertiary_delim', None)

        self.train_data_format = conf_dict.get("train_data_format", None)
        self.infer_data_format = conf_dict.get("infer_data_format", None)

        # data dir
        self.train_data = walk_path(conf_dict['train_data'])
        self.eval_data = walk_path(conf_dict['eval_data'])
        self.pred_data = walk_path(conf_dict['pred_data']) if 'pred_data' in conf_dict else []
        self.data_path = {'train_path': self.train_data, 'eval_path': self.eval_data, 'pred_data': self.pred_data}
        self.data_type = conf_dict["data_type"]

        assert len(self.train_data) > 0, "can't find training data in {}".format(conf_dict['train_data'])
        assert len(self.eval_data) > 0, "can't find evaluation data in {}".format(conf_dict['eval_data'])

    def split_data(self, part_index, part_num):
        if len(self.train_data) > part_num:
            all_file_num = len(self.train_data)
            self.train_data = self.train_data[part_index::part_num]
            train_data_part = all_file_num // len(self.train_data)
        else:
            train_data_part = 1
        if len(self.eval_data) > part_num:
            self.eval_data = self.eval_data[part_index::part_num]
        if len(self.pred_data) > part_num:
            self.pred_data = self.pred_data[part_index::part_num]
        return train_data_part

    def __repr__(self):
        return json.dumps(
            {'train_data': self.train_data, 'evaluation_data': self.eval_data, 'data_type': self.data_type,
             'schema': self.schema, 'labels': self.labels})

    def __getitem__(self, item):
        return self.__getattribute__(item)


class ModelConfig(object):
    def __init__(self, conf_dict):
        self.partition = conf_dict['partition']
        self.input_node_map = conf_dict.get("input_node_map", None)
        self.out_node_names = conf_dict.get("out_node_names", None)
        self.batch_size = conf_dict["batch_size"]
        self.optimizer = conf_dict.get('optimizer').lower()
        self.learning_rate = conf_dict['learning_rate']
        self.lr_decay_steps = conf_dict['lr_decay_steps']
        self.lr_decay_rate = conf_dict['lr_decay_rate']
        self.deep_learning_rate = conf_dict['deep_learning_rate']
        self.l1 = conf_dict['l1']
        self.l2 = conf_dict['l2']
        self.reduce_sequence_length = conf_dict['reduce_sequence_length']
        self.init_std = conf_dict['init_std']
        self.dropout = conf_dict['dropout']
        self.last_dropout = conf_dict['last_dropout']
        self.hidden_num = conf_dict['hidden_num']
        self.wd = conf_dict['wd']
        self.embedding_dim = conf_dict['embedding_dim']

    def __repr__(self):
        return json.dumps({'lr': self.learning_rate, 'deep_lr': self.deep_learning_rate, 'optimizer': self.optimizer,
                           'l1': self.l1, 'l2': self.l2, 'dropout': self.dropout, 'last_dropout': self.last_dropout})


class MissionConfig(object):

    def __init__(self, conf_dict):
        self.run_config = RunConfig(conf_dict["runconfig"])
        self.train_config = TrainConfig(conf_dict["train"])

        self.model_name = self.run_config.model_name

    def __repr__(self):
        return json.dumps({'model': self.run_config.model_name, 'batch_size': self.run_config.batch_size,
                           'max_steps': self.run_config.max_steps, 'max_epoch': self.run_config.max_epoch,
                           'validation_sample': self.run_config.validation_sample, 'log_dir': self.train_config.log_dir,
                           'profile_dir': self.train_config.profile_dir})


class TrainConfig(object):
    def __init__(self, conf_dict):
        # model dir
        self.date = conf_dict['date']
        self.version = conf_dict['version']
        self.task_flag_dir = make_dir(conf_dict['flag_dir'])
        self.log_dir = make_dir(conf_dict['log_dir'].format(self.date, self.version))
        self.flag_dir = make_dir(os.path.join(self.log_dir, "flag"))
        self.ckpt_dir = make_dir(os.path.join(self.log_dir, "ckpt"))
        self.result_dir = make_dir(conf_dict['result_dir'].format(self.date, self.version))
        self.graph_dir = make_dir(conf_dict['graph_dir'].format(self.date, self.version))
        self.online_graph_dir = make_dir(conf_dict['online_graph_dir'].format(self.date, self.version))
        self.profile_dir = make_dir(conf_dict['profile_dir'].format(self.date, self.version))

        self.id_file_dir = conf_dict['id_file_path']
        self.export_dir = conf_dict['export_dir']

        # self.optional = conf_dict


class RunConfig(object):
    def __init__(self, conf_dict):
        self.model_name = conf_dict["model_name"]
        self.tmp_path = make_dir(conf_dict['tmp_path'], True)

        self.distribute = conf_dict["distribute"]
        self.batch_size = conf_dict['batch_size']
        self.train_data_size = conf_dict.get("train_data_size", 10000)
        self.max_steps = conf_dict["max_steps"]
        self.max_epoch = conf_dict['max_epoch']
        self.nthreads = conf_dict['nthreads']
        self.eval_period = conf_dict['eval_period']
        self.profile_period = conf_dict['profile_period']
        self.log_period = conf_dict['log_period']
        self.validation_sample = conf_dict['validation_sample']
        self.print_timeline = conf_dict['print_timeline']
        self.print_steps = conf_dict['print_steps']
        self.train_eval_num = conf_dict['train_eval_num']


def walk_path(path):
    return [os.path.join(pathStr(path), file) for file in wfile.ListDirectory(pathStr(path)) if not file.startswith((".", "_"))]


def make_dir(path, clear=False):
    path = pathStr(path)
    if not wfile.Exists(path):
        wfile.MakeDirs(path)
    elif clear:
        wfile.DeleteRecursively(path)
        wfile.MakeDirs(path)
    return str(path)
