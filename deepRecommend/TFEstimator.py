import json
import os

import tensorflow as tf

import numpy as np
from tensorflow.python.framework import graph_util


from file.wfile import wfile
from utils.util import Timer, tensorflow_save_parameters_with_partition


class Environment(object):
    def __init__(self, cluster_ips):
        self.cluster = self.dispatch_task(cluster_ips)
        if self.cluster['task']['type'] == 'local':
            self.local_mode = True
            self.task_index = 0
            self.job_name = "worker"
            self.is_chief = True
            self.ps_num = 1
            self.worker_num = 1
            self.server = None
            # self.shard = [0, 1]
        else:
            self.local_mode = False
            self.task_index = self.cluster['task']['index']
            self.job_name = self.cluster['task']['type']
            self.is_chief = self.cluster['task']['type'] == 'chief'
            self.ps_num = len(self.cluster['cluster']['ps'])
            self.worker_num = len(self.cluster['cluster']['worker']) + 1
            # self.shard = [self.task_index + int(self.is_chief) * (self.worker_num - 1), self.worker_num]

    def dispatch_task(self, ips):

        if len(ips['cluster_ips']) != 1:
            num_ps = ips['num_ps']
            local_ip = ips['local_ip']
            cluster_ips = ips['cluster_ips']
            num_worker = len(cluster_ips) - num_ps - 1
            ip_idx = cluster_ips.index(local_ip)
            if ip_idx > num_worker:
                task_type, task_index = 'ps', ip_idx - num_worker - 1
            # elif ip_idx == num_ps:
            #     task_type, task_index = 'chief', ip_idx-num_ps

            elif ip_idx == num_worker:
                task_type, task_index = 'evaluator', 0
            elif ip_idx == 0:
                task_type, task_index = 'chief', ip_idx
            else:
                task_type, task_index = 'worker', ip_idx - 1
            tf_config = {'cluster': {"ps": [ip + ":2222" for ip in cluster_ips[1 + num_worker:]],
                                     'worker': [ip + ":2222" for ip in cluster_ips[1:num_worker]],
                                     'chief': [cluster_ips[0] + ":2222"]},
                         'task': {'type': task_type, 'index': task_index}}
            print("tf_config: {}".format(tf_config))
            os.environ['TF_CONFIG'] = json.dumps(tf_config)
            return tf_config

        else:
            return {'task': {'type': 'local', 'index': 0}}


class Estimator(object):
    def __init__(self, local_mode, job, model, mission_conf, data_config):
        self.local_mode = local_mode
        self.model = model
        self.job = job
        self.start_epoch = 0
        self.data_config = data_config
        self.train_config = mission_conf.train_config
        self.run_config = mission_conf.run_config

        self.estimator = self.build_estimator(self.data_config, self.run_config)

    def build_estimator(self, data_conf, run_config):
        self.model.label_name = data_conf.labels
        config = tf.compat.v1.ConfigProto(inter_op_parallelism_threads=0,
                                          intra_op_parallelism_threads=0,
                                          log_device_placement=True,
                                          allow_soft_placement=True
                                          # device_count = {"GPU": 1}  # limit to GPU usage
                                          )

        tf_run_config = tf.estimator.RunConfig(
            **{"save_checkpoints_steps": run_config.eval_period, "log_step_count_steps": 5000,
               "train_distribute": None if self.local_mode or self.job == 'evaluator' else tf.contrib.distribute.ParameterServerStrategy(),
               "keep_checkpoint_max": 5}
        ).replace(session_config=config)

        params = {
            'data_config': data_conf,
            'run_config': run_config
        }

        return tf.estimator.Estimator(
            model_dir=self.train_config.ckpt_dir,
            model_fn=self.model_fn,
            params=params,
            config=tf_run_config
        )

    def model_fn(self, features, labels, mode, params):
        """Model function used in the estimator.
        Args:
            features (Tensor): Input features to the model.
            labels (Tensor): Labels tensor for training and evaluation.
            mode (ModeKeys): Specifies if training, evaluation or prediction.
            params (HParams): hyperparameters.
        Returns:
            (EstimatorSpec): Model to be run by Estimator.
        """
        is_training = mode == tf.estimator.ModeKeys.TRAIN
        self.model.is_train = is_training
        self.model.build_graph(features, labels, params['data_config'], params['run_config'])
        if mode == tf.estimator.ModeKeys.PREDICT:
            output_node = tf.get_default_graph().get_tensor_by_name(self.model.out_node + ":0")
            return tf.estimator.EstimatorSpec(mode, predictions=output_node)  # , export_outputs=export_outputs)

        self.model.build_train_op(params['run_config'])

        eval_metric_ops = self.model.get_eval_metric_ops(is_training, field=mode)

        if mode == tf.estimator.ModeKeys.EVAL:
            return tf.estimator.EstimatorSpec(mode, loss=self.model.loss, eval_metric_ops=eval_metric_ops)

        hook = [tf.estimator.ProfilerHook(save_steps=params['run_config'].profile_period,
                                          output_dir=self.train_config.profile_dir,
                                          show_memory=True)]

        if mode == tf.estimator.ModeKeys.TRAIN:
            return tf.estimator.EstimatorSpec(mode, loss=self.model.loss,
                                              train_op=tf.group(*self.model.trainer) if type(
                                                  self.model.trainer) == list else self.model.trainer,
                                              training_hooks=hook)

    def train_and_eval(self):
        train_epochs = self.run_config.max_epoch
        max_steps = self.run_config.max_steps
        steps = 1000  # self.run_config.eval_period

        train_data = self.data_config.train_data
        eval_data = self.data_config.eval_data

        batch_size = self.run_config.batch_size

        # self.model.build_input(self.data_config, self.run_config, [task_index, task_num])

        # predict_batch(train_data, parse_fn)
        train_spec = tf.estimator.TrainSpec(
            input_fn=lambda: self.model.build_train_input(self.data_config, self.run_config, self.run_config.max_epoch)
            # .prefetch(buffer_size=self.model.model_conf.batch_size*1000)
            , max_steps=max_steps)
        eval_spec = tf.estimator.EvalSpec(
            input_fn=lambda: self.model.build_eval_input(self.data_config, self.run_config),
            steps=steps, throttle_secs=600)

        t0 = Timer()

        tf.estimator.train_and_evaluate(self.estimator, train_spec, eval_spec)

        tf.compat.v1.logging.info(
            '<EPOCH {}>: Finish evaluation {}, take {} mins'.format(train_epochs + 1, eval_data, t0.elapsed()))
        return 0

    def test(self):
        predict_input = lambda: self.model.build_test_input(self.data_config, self.run_config)
        out = self.estimator.predict(predict_input)

        for _ in range(100000):
            a = next(out)
            print(a)
        print("stop")

    def export_model(self, data_config):
        model_dir = os.path.join(self.train_config.log_dir,
                                 'save_predict_model')  # # '/Users/snoworday/data/wide_deep/experiment/2019-02-17/log_data/save_predict_model'
        variable_dir = self.train_config.log_dir
        if self.model.out_node is not None:
            def serverWrapper(model_input_fn):
                def wrapper():
                    feature_input, _ = model_input_fn()
                    return tf.estimator.export.ServingInputReceiver(features=feature_input,
                                                                    receiver_tensors=feature_input)

                return wrapper

            if wfile.Exists(model_dir):
                wfile.DeleteRecursively(model_dir); wfile.MakeDirs(model_dir)
            org_model_dir_set = set(wfile.ListDirectory(model_dir))
            self.estimator.export_saved_model(model_dir, serverWrapper(self.model.build_export_input), as_text=False)
            new_model_dir = os.path.join(model_dir, (set(wfile.ListDirectory(model_dir)) - org_model_dir_set).pop())
            self.freezy_model(model_dir, self.model.out_node, new_model_dir)
            # wfile.Copy(os.path.join(new_model_dir, 'saved_model.pb'), os.path.join(model_dir, 'model.pb'))
            # wfile.DeleteRecursively(new_model_dir)
            # copy model.pb
            save_model_path = os.path.join(self.train_config.online_graph_dir, 'model')
            if wfile.Exists(save_model_path): wfile.DeleteRecursively(save_model_path)
            wfile.MakeDirs(save_model_path)
            wfile.Copy(os.path.join(model_dir, "model.pb"), os.path.join(save_model_path, "model.pb"))

        self.save_variables(variable_dir, "txt")
        self.model.union_info_weight(self.train_config.id_file_dir, self.train_config.online_graph_dir,
                                     data_config.bucket_info, variable_dir, model_dir,
                                     self.train_config.online_graph_dir)

        if wfile.Exists(self.train_config.export_dir): wfile.DeleteRecursively(self.train_config.export_dir)
        wfile.CopyDirectory(self.train_config.online_graph_dir, self.train_config.export_dir)
        with wfile.Open(os.path.join(self.train_config.export_dir, "UPDATE_LOCK"), "w") as lock_file:
            lock_file.write("done")
        return 0

    def save_variables(self, save_dir, filetype="txt"):
        self.model.is_train = False
        self.model.build_embedding_params()
        ckpt = tf.train.get_checkpoint_state(self.train_config.ckpt_dir)
        saver = tf.train.Saver()
        with tf.Session() as sess:
            saver.restore(sess, ckpt.model_checkpoint_path)
            tensorflow_save_parameters_with_partition(sess, save_dir, out_type=filetype,
                                                      tmp_dir=self.run_config.tmp_path)


    def freezy_model(self, save_predict_dir, out_node_names, graph_pb_path,
                             output_graph_name='model.pb'):
        def tensorflow_set_weights(sess, weights):
            assign_ops = []
            feed_dict = {}
            vs = {v.name: v for v in tf.compat.v1.get_default_graph().get_collection(tf.compat.v1.GraphKeys.VARIABLES)}
            for vname, v in vs.items():
                value = np.asarray(weights[vname])
                assign_placeholder = tf.compat.v1.placeholder(v.dtype, shape=v.shape)
                assign_op = v.assign(assign_placeholder)
                assign_ops.append(assign_op)
                feed_dict[assign_placeholder] = value
            sess.run(assign_ops, feed_dict=feed_dict)

        def tensorflow_get_weights(sess, graph):
            vs = {v.name: v for v in graph.get_collection(
                tf.compat.v1.GraphKeys.VARIABLES)}
            name_values = sess.run(vs)
            return name_values
        if self.model.out_node is not None:
            assert graph_pb_path != '', 'Give me ur graph and vars, OAO'
            out_node_names = out_node_names.split(",")
            if not wfile.IsDirectory(save_predict_dir):
                wfile.MakeDirs(save_predict_dir)
            org_graph = tf.Graph()
            new_graph = tf.Graph()
            # read graph
            with tf.Session(graph=org_graph) as sess:
                org_meta_graph_def = tf.compat.v1.saved_model.loader.load(sess, [
                    tf.compat.v1.saved_model.tag_constants.SERVING], graph_pb_path)
                weights = tensorflow_get_weights(sess, org_graph)

            # set placeholder & write graph->pb/variables->ckpt
            with tf.Session(graph=new_graph) as sess:
                _ = tf.train.import_meta_graph(org_meta_graph_def, import_scope="", return_elements=out_node_names,
                                               clear_devices=True)

                tensorflow_set_weights(sess, weights)
                saver = tf.train.Saver()
                output_graph_def = graph_util.convert_variables_to_constants(
                    sess,
                    tf.get_default_graph().as_graph_def(),
                    out_node_names
                )
                output_graph_path = os.path.join(save_predict_dir, output_graph_name)
                if wfile.Exists(output_graph_path):
                    wfile.Remove(output_graph_path)
                with wfile.Open(output_graph_path, 'wb') as f:
                    f.write(output_graph_def.SerializeToString())

    def _model_pb_cal(self):
        import tensorflow as tf
        from tensorflow.python.platform import gfile
        GRAPH_PB_PATH = '/Users/snoworday/data/wide_deep/experiment/2020-02-16/base/logData/save_predict_model/model.pb'
        sess = tf.Session()
        print("load graph")
        with gfile.FastGFile(GRAPH_PB_PATH, 'rb') as f:
            graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        sess.graph.as_default()
        tf.import_graph_def(graph_def, name='')
        graph_nodes = [n for n in graph_def.node]
        names = []
        for t in graph_nodes:
            names.append(t.name)
        print(names)
        feed_dict = {"":np.array([])}
        result = sess.run("output_layer/predict_value:0", feed_dict=feed_dict)

