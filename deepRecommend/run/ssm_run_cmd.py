import boto3

task_bash_path = "s3://jiayun-recommend/private/wangqi/wide_deep/train_bash/ssm_run.sh"
instance_num = 40
task_param = dict()
task_param['task_bash_path'] = "/home/hadoop/{}".format(task_bash_path.split('/')[-1])
task_param['flag_path'] = "s3://jiayun-recommend/private/wangqi/wide_deep/task/flag"
task_param['date'] = "2020-01-13"
task_param['task_config_path'] = "s3://jiayun-recommend/private/wangqi/wide_deep/task/config"
task_param['version'] = "V1"
task_param['ps_num'] = 16
task_param['mode'] = "train_and_export"

emr_client = boto3.client('emr', 'us-west-2')
ssm_client = boto3.client('ssm', 'us-west-2')
cluster_id = "j-37J4GM3YQ5KRE"
cluster_info = emr_client.describe_cluster(ClusterId=cluster_id)

instance_ids = [item['Ec2InstanceId'] for item in emr_client.list_instances(ClusterId=cluster_id)['Instances']]
task_param['ips'] = ','.join([item['PrivateIpAddress'] for item in emr_client.list_instances(ClusterId=cluster_id)['Instances']])

cmd = []
cmd.append('sudo -u hadoop aws s3 cp {} /home/hadoop'
           .format(task_bash_path))
cmd.append('sudo chmod +x {}'.format(task_param['task_bash_path']))
cmd.append('sudo -u hadoop {task_bash_path} {date} {mode} {task_config_path} {version} {ips} {ps_num} {flag_path}'.format(**task_param))

resp = ssm_client.send_command(
    DocumentName="AWS-RunShellScript",
    Parameters={'commands': cmd},
    OutputS3BucketName='jiayun-recommend',
    OutputS3KeyPrefix='private/wangqi/wide_deep/trash',
    InstanceIds=instance_ids,
    TimeoutSeconds=3600
)

# first_command_status = ssm_client.list_commands(CommandId=first_command)['Commands'][0]['Status']
#
# ssm_client.cancel_command(CommandId=first_command, InstanceIds=instance_ids)
#
#
# date = 2020-01-10
# mode = train_and_export
# task_config_path = s3://jiayun-recommend/private/wangqi/wide_deep/task/config
# instance_num = 20
# ps_num = 10
#
# >>> resp
# {'Command': {'CommandId': '5c6e4568-0bbd-4805-ae0a-f877962b22e8', 'DocumentName': 'AWS-RunShellScript', 'DocumentVersion': '', 'Comment': '', 'ExpiresAfter': datetime.datetime(2020, 1, 14, 8, 53, 27, 809000, tzinfo=tzlocal()), 'Parameters': {'commands': ['sudo -u hadoop aws s3 cp s3://jiayun-recommend/private/wangqi/wide_deep/train_bash/ssm_run.sh /home/hadoop', 'sudo chmod +x /home/hadoop/ssm_run.sh', 'sudo -u hadoop /home/hadoop/ssm_run.sh 2020-01-10 train_and_export s3://jiayun-recommend/private/wangqi/wide_deep/task/config V1 172.31.26.100,172.31.31.11,172.31.28.179,172.31.26.152,172.31.23.255,172.31.31.178,172.31.26.52,172.31.21.211,172.31.25.156,172.31.29.113,172.31.20.116,172.31.16.68,172.31.24.211,172.31.26.33,172.31.20.13,172.31.30.145,172.31.25.197,172.31.23.103,172.31.31.239,172.31.24.0,172.31.23.8,172.31.27.210,172.31.25.122,172.31.29.54,172.31.22.117,172.31.23.195,172.31.27.32,172.31.23.105,172.31.20.88,172.31.31.26,172.31.28.226,172.31.18.203,172.31.30.79,172.31.28.183,172.31.18.144,172.31.21.231,172.31.30.13,172.31.16.178,172.31.29.236,172.31.21.199,172.31.31.35 16']}, 'InstanceIds': ['i-07de16de630b529c4', 'i-0265df92ee4c3ca95', 'i-03815225c9026ddda', 'i-0f6b918d4d8a287a1', 'i-0830e57723ffe90ac', 'i-0319fe1439174f735', 'i-0ef829f83393f4e6c', 'i-0841515a6bc686479', 'i-02f3c59c8d0e17287', 'i-089efe9c7082ee17e', 'i-00243449b54a52aec', 'i-0a428a7469bc7010e', 'i-097710046ea92d1bc', 'i-050fabf1fc1fe3fbb', 'i-0fb6bb1ee65c0b073', 'i-065faeaf1db291a2a', 'i-02993870208a2b48f', 'i-079639feb2dea9583', 'i-088cf3693f0b57cec', 'i-0d321b0dac8d5e6cf', 'i-06c86803969f00956', 'i-018a0b4846ad4c810', 'i-05acc25afa4c3a679', 'i-00d97cda868f41d8e', 'i-079050e711bab13be', 'i-0017ad519d57abb02', 'i-043b8ee885a305d58', 'i-06812d8f8a35adf2b', 'i-0748df121129df9a1', 'i-0325ea35d4b02afd7', 'i-0042faf18cb7b5fd1', 'i-05cd5005ed6de3b34', 'i-0d1338c88cabbceb9', 'i-077f344f3f08f1bd8', 'i-0a007cb042f453ef9', 'i-0a31ec45c63fcffb4', 'i-0e00751d544a4d9f0', 'i-089962c958e7ec560', 'i-065582eaace929d1b', 'i-0800b470b301acb32', 'i-034d3a782d41d30f8'], 'Targets': [], 'RequestedDateTime': datetime.datetime(2020, 1, 14, 6, 53, 27, 809000, tzinfo=tzlocal()), 'Status': 'Pending', 'StatusDetails': 'Pending', 'OutputS3BucketName': 'jiayun-recommend', 'OutputS3KeyPrefix': 'private/wangqi/wide_deep/trash', 'MaxConcurrency': '50', 'MaxErrors': '0', 'TargetCount': 41, 'CompletedCount': 0, 'ErrorCount': 0, 'DeliveryTimedOutCount': 0, 'ServiceRole': '', 'NotificationConfig': {'NotificationArn': '', 'NotificationEvents': [], 'NotificationType': ''}, 'CloudWatchOutputConfig': {'CloudWatchLogGroupName': '', 'CloudWatchOutputEnabled': False}}, 'ResponseMetadata': {'RequestId': '50d7988a-25c9-47d9-9bbc-3099785aca81', 'HTTPStatusCode': 200, 'HTTPHeaders': {'x-amzn-requestid': '50d7988a-25c9-47d9-9bbc-3099785aca81', 'content-type': 'application/x-amz-json-1.1', 'content-length': '2470', 'date': 'Tue, 14 Jan 2020 06:53:27 GMT'}, 'RetryAttempts': 0}}