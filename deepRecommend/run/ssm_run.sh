#!/usr/bin/env bash
source /etc/bashrc
source /home/hadoop/.bashrc

cd /home/hadoop

date=$1
mode=$2
experiment_config=$3
experiment_version=$4
cluster_ips=$5
ps_num=$6
flag_path=$7

echo ${cluster_ips}
echo "finish"
if [[ ! -d "algorithm-deeplearning" ]]; then
    git clone -b develop https://qiwang1@bitbucket.org/kaifeiyao/algorithm-deeplearning.git
fi

project_dir=$(cd algorithm-deeplearning/deepRecommend && pwd)

aws s3 cp ${experiment_config} ./experiment_config

data_config=$(jq -r ".data_config" "experiment_config")
train_config=$(jq -r ".train_config" "experiment_config")
experiment_path=$(jq -r ".experiment_path" "experiment_config")
export_path=$(jq -r ".export_path" "experiment_config")
data_version=$(jq -r ".${experiment_version}.data_version" "experiment_config")
train_version=$(jq -r ".${experiment_version}.train_version" "experiment_config")


# redirect to your path
## read
parameter_path="${train_config}/${train_version}.json"
conf_path="${data_config}/${data_version}/profile/${date}.json"
## write
#experiment_tmp_path="hdfs:///user/hadoop/experiment"
#experiment_path="${experiment_path}"
#export_path="s3://jiayun-recommend/common/deep_wide/pre"


# ./submit_run.sh 2020-01-10 train_export s3://jiayun-recommend/private/wangqi/wide_deep/task/config V1 16


local_ip=$(/sbin/ifconfig | grep 'inet addr:172' | awk '{print $2}' | sed 's/addr://g')

ps -ef | grep python | grep train | awk '{print "kill -9 " $2}' | bash -v
cd ${project_dir}
echo ${cluster_ips}

python3 -u main.py \
   --mode ${mode} \
   --experiment_path ${experiment_path} \
   --version ${experiment_version} \
   --num_ps ${ps_num} \
   --local_ip ${local_ip} \
   --cluster_ips ${cluster_ips} \
   --partition ${ps_num} \
   --conf_path ${conf_path} \
   --parameter_path ${parameter_path} \
   --export_path ${export_path} \
   --date ${date} \
   --flag_path ${flag_path}
ret=$?
if [[ $ret -ne 0 ]]
then
        echo "task wide_distribute_train exit with error code $ret"
        exit $ret
fi
