#!/usr/bin/env bash
PATH=/bin:/sbin:/usr/bin

date=$1
mode=$2
experiment_config=$3
experiment_version=$4
ps_num=$5
cluster_ips=$6

aws s3 cp ${experiment_config} ./experiment_config

data_config=$(jq -r ".data_config" "experiment_config")
train_config=$(jq -r ".train_config" "experiment_config")
experiment_path=$(jq -r ".experiment_path" "experiment_config")
export_path=$(jq -r ".export_path" "experiment_config")
data_version=$(jq -r ".${experiment_version}.data_version" "experiment_config")
train_version=$(jq -r ".${experiment_version}.train_version" "experiment_config")

# redirect to your path
## read
parameter_path="${train_config}/${train_version}.json"
conf_path="${data_config}/${data_version}/profile/${date}.json"
## write
#experiment_path="${experiment_path}"
#export_path="s3://jiayun-recommend/common/deep_wide/pre"

project_dir=$(cd `dirname $0` && cd .. && pwd)

# bash deepRecommend/submit_run.sh /home/hadoop/wangqi.pem 2020-02-11 train_and_export s3://jiayun-recommend/private/wangqi/wide_deep/task/config V2 run 12

local_ip=$(/sbin/ifconfig | grep 'inet addr:172' | awk '{print $2}' | sed 's/addr://g')

ps -ef | grep python | grep train | awk '{print "kill -9 " $2}' | bash -v
cd ${project_dir}
echo ${cluster_ips}

nohup python3 -u solo.py \
   --mode ${mode} \
   --experiment_path ${experiment_path} \
   --version ${experiment_version} \
   --num_ps ${ps_num} \
   --local_ip ${local_ip} \
   --cluster_ips ${cluster_ips} \
   --partition ${ps_num} \
   --conf_path ${conf_path} \
   --parameter_path ${parameter_path} \
   --export_path ${export_path} \
   --date ${date} > ${mode}.log &
