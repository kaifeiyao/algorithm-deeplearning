import math
import os
import time
from functools import wraps

import tensorflow as tf
from tensorflow.python.framework import graph_util
from tensorflow.python.profiler import model_analyzer

from file.wfile import wfile
from utils.graph_handle import GraphHandler, timeline_profile, PerformRecoder
from utils.util import Timer, tensorflow_save_parameters_with_partition


class Environment(object):
    def __init__(self, cluster_ips):
        tf_config = self.dispatch_task(cluster_ips)
        if tf_config['task_type'] == 'local':
            self.local_mode = True
            self.cluster = None
            self.task_index = 0
            self.job_name = "worker"
            self.is_chief = True
            self.ps_num = 1
            self.worker_num = 1
        else:
            self.local_mode = False
            self.cluster = tf.train.ClusterSpec({'ps': [ip + ":2222" for ip in tf_config['ps']],
                                                 'worker': [ip + ":2222" for ip in tf_config['worker']]})
            self.task_index = tf_config['task_index']
            self.job_name = tf_config['task_type']
            self.is_chief = self.task_index == 0 and self.job_name == "worker"  # 'chief'
            self.ps_num = len(tf_config['ps'])
            self.worker_num = len(tf_config['worker'])

    def __config_server(self):
        if self.local_mode:
            self.server = None
        else:
            self.server = tf.train.Server(self.cluster, job_name=self.job_name, task_index=self.task_index,
                                          config=tf.ConfigProto(allow_soft_placement=True))

    def dispatch_task(self, ips):

        if len(ips['cluster_ips']) != 1:
            num_ps = ips['num_ps']
            local_ip = ips['local_ip']
            cluster_ips = ips['cluster_ips']
            num_worker = len(cluster_ips) - num_ps
            ip_idx = cluster_ips.index(local_ip)
            if ip_idx >= num_worker:
                task_type, task_index = 'ps', ip_idx - num_worker
            # elif ip_idx == num_ps:
            #     task_type, task_index = 'chief', ip_idx-num_ps
            else:
                task_type, task_index = 'worker', ip_idx  # -1
            tf_config = {"ps": cluster_ips[num_worker:], 'worker': cluster_ips[:num_worker],
                         # , 'chief': [cluster_ips[num_ps]],
                         'task_type': task_type, 'task_index': task_index}
            print("tf_config: {}".format(tf_config))
            return tf_config

        else:
            return {'task_type': 'local', "task_index": 0, "worker": ['local']}

    def distribution(self, task_func):
        is_ps = self.job_name == 'ps'
        task_index = self.task_index
        cluster = self.cluster
        self.__config_server()

        @wraps(task_func)
        def wrapper(*args, **kwargs):
            if is_ps:
                self.server.join()
            else:
                if cluster is None:
                    exit_code = task_func(*args, **kwargs)
                    return exit_code
                else:
                    time.sleep(60)
                    with tf.device(tf.train.replica_device_setter(worker_device=("/job:worker/task:%d" % task_index),
                                                                  cluster=cluster)):
                        exit_code = task_func(*args, **kwargs)
                        return exit_code

        return wrapper


class Estimator(object):
    def __init__(self, local_mode, is_train, model, mission_conf, data_conf):
        self.local_mode = local_mode
        self.model = model
        self.start_epoch = 0
        self.train_config = mission_conf.train_config
        self.data_conf = data_conf
        self.run_config = mission_conf.run_config
        self.model.graph = tf.get_default_graph()
        if is_train:
            self.model.is_train = tf.placeholder(tf.bool, [], name='is_train')
        else:
            self.model.is_train = False
        features, label = self.model.build_input(self.data_conf, self.run_config)
        self.model.build_graph(features, label, data_conf, self.run_config)

    def initialize_session(self, server, is_chief, run_conf, sync_replica):

        """Start computational session on builded graph.
        """
        config = tf.ConfigProto(allow_soft_placement=True, log_device_placement=True, inter_op_parallelism_threads=0,
                                intra_op_parallelism_threads=0)
        self.model.get_eval_metric_ops(True, "train")
        self.model.build_train_op(run_conf, sync_replica)
        self.saver = tf.train.Saver()
        with self.model.graph.as_default():
            # profie hook
            if not self.local_mode:
                self.train_config.profile_dir = os.path.join(self.train_config.profile_dir, str(sync_replica))
                if not wfile.Exists(self.train_config.profile_dir):
                    wfile.MakeDirs(self.train_config.profile_dir)
                hooks = [
                    tf.train.ProfilerHook(save_steps=run_conf.profile_period, output_dir=self.train_config.profile_dir,
                                          show_memory=True,
                                          show_dataflow=True)]
                if self.run_config.distribute['sync_train']:
                    sync_replicas_hook = self.model.optimizer.make_session_run_hook(is_chief=is_chief,
                                                                                    num_tokens=0)
                    hooks.append(sync_replicas_hook)
                self.session = tf.train.MonitoredTrainingSession(master=server.target,
                                                                 is_chief=is_chief,
                                                                 checkpoint_dir=self.train_config.ckpt_dir,
                                                                 save_checkpoint_steps=None,
                                                                 # forbbdien the checkpoint savers
                                                                 save_checkpoint_secs=None,
                                                                 save_summaries_steps=None,
                                                                 save_summaries_secs=None,
                                                                 hooks=hooks)
            else:
                print("use local session")
                self.session = tf.Session(config=config)
                # self.session.run(self.model.init_all_vars)
            ckpt = tf.train.get_checkpoint_state(self.train_config.ckpt_dir)
            if ckpt and ckpt.model_checkpoint_path:
                self.saver.restore(self.session, ckpt.model_checkpoint_path)
                print("restore success from {}".format(ckpt.model_checkpoint_path))
                self.start_epoch = int(
                    ckpt.model_checkpoint_path.split('/')[-1].split('.')[0].split('_')[-1].split('-')[0]) + 1
                self.start_step = int(
                    ckpt.model_checkpoint_path.split('/')[-1].split('.')[0].split('_')[-1].split('-')[1])
            else:
                self.session.run(self.model.init_all_vars)  # feed_dict={self.model.embedding_init:emb_dict})
                self.start_step = 0

        print("===runing graph:", self.session.graph)
        if self.model.mode == 'train':
            self.test_handle = self.session.run(self.model.test_iterator.string_handle())
            self.training_handle = self.session.run(self.model.training_iterator.string_handle())
            self.validation_handle = self.session.run(self.model.validation_iterator.string_handle())

    def train_and_eval(self, task_index, worker_num, is_chief):
        self.writer = tf.summary.FileWriter(logdir=self.train_config.profile_dir, graph=tf.get_default_graph())
        "Read data iteration control parameters"
        self.run_config.eval_period //= worker_num
        steps_per_epoch = int(math.ceil(1.0 * self.run_config.train_data_size / self.model.model_conf.batch_size))
        if steps_per_epoch * self.start_epoch > self.run_config.max_steps:
            self.steps = self.run_config.max_steps
        else:
            self.steps = steps_per_epoch * self.start_epoch
        batch_num = int(self.run_config.train_data_size / self.model.model_conf.batch_size)

        "Model save management"
        perform_recoder = PerformRecoder(self.saver, 1)
        self.graph_handler = GraphHandler(self.model, self.session, self.train_config.ckpt_dir)
        timer = Timer()

        "Profiler show model structure"
        print("Profiler show model structure:")
        # show_model_structure(self.session)

        "Timeline configuration"
        if self.train_config.profile_dir is not None:
            self.model_profiler = model_analyzer.Profiler(graph=self.session.graph)
            self.run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
            self.run_metadata = tf.RunMetadata()

        "According to the number of iterations and global_step control"
        ops_to_run = [self.model.trainer, self.model.loss, self.model.assert_op, self.model.summary,
                      self.model.global_step]

        self.session.run(self.model.validation_iterator.initializer)
        i_epoch = self.start_epoch
        global_step = 0
        for i_epoch in range(self.start_epoch, self.run_config.max_epoch):
            self.try_times = 0
            self.session.run(self.model.training_iterator.initializer)

            # # 第3轮后，wide停止更新，只训练deep
            # if i_epoch == 3:
            #     ops_to_run = [self.model.trainer_deep, self.model.wide_loss, self.model.wide_loss_sum,
            #                   self.model.deep_loss, self.model.deep_loss_sum, self.model.weight_sum,
            #                   self.model.assert_op, self.model.summary]
            #     print("第3轮后，wide停止更新，只训练deep")
            while True:
                try:
                    idx_batch = self.steps % batch_num
                    _, loss, assertion, summary_op, global_step = self.session.run(ops_to_run,
                                                                                   feed_dict={
                                                                                       self.model.handle: self.training_handle,
                                                                                       self.model.is_train: True},
                                                                                   options=self.run_options,
                                                                                   run_metadata=self.run_metadata)

                    if self.steps % self.run_config.log_period == 0 or self.steps == 1:

                        print('sample epoch:%d: %d/%d, local step:%d, duration:%.4f -- loss:%.4f'
                              % (i_epoch, idx_batch, batch_num, self.steps, timer.elapsed(), loss))
                        if self.train_config.profile_dir is not None and self.run_config.print_timeline:
                            train_writer = tf.summary.FileWriter(self.train_config.profile_dir, self.session.graph)
                            timeline_profile(self.model_profiler, self.steps, self.train_config.profile_dir,
                                             self.run_metadata, train_writer)
                        self.writer.add_summary(summary_op, global_step)
                    # self.writer.flush()
                    "The last second rounds begin to evaluate the training set and validation set, " \
                    "and save the model structure according to the verification AUC level."

                    if self.run_config.validation_sample and self.steps > int(self.run_config.print_steps) and (
                            i_epoch >= 0) and (
                            self.steps % self.run_config.eval_period == 0):
                        # print('sample epoch:%d: %d/%d, global step:%d, duration:%.4f -- loss:%.4f'
                        #       % (i_epoch, idx_batch, batch_num, self.steps, timer.elapsed(), loss))

                        # ---- train ----
                        self.model.get_evaluation(self.session, self.training_handle,
                                                  self.run_config.train_eval_num, "train")
                        self.model.get_evaluation(self.session, self.validation_handle,
                                                  self.run_config.train_eval_num, "eval")
                        print("global_step: ", global_step)

                        if is_chief:
                            # global_step_tensor = training_util._get_or_create_global_step_read(self.session.graph)
                            # global_step_all = self.session.run(global_step_tensor)
                            save_path = self.saver.save(self.graph_handler.get_session(self.session),
                                                        self.train_config.ckpt_dir + '/epoch_' + str(i_epoch),
                                                        global_step=global_step)
                        timer.elapsed()
                    self.steps += 1
                    # global_step += 1

                except tf.errors.OutOfRangeError:
                    "Save and aggregate worker state after iterating round."
                    print('Done training for %d epochs, %d steps.' % (i_epoch, self.steps))
                    "Decide whether to test samples after training."
                    # global_step_tensor = training_util._get_or_create_global_step_read(self.session.graph)
                    # global_step_all = self.session.run(global_step_tensor)
                    print('cfg.validation_sample', self.run_config.validation_sample)
                    if self.run_config.validation_sample:
                        val_auc = self.model.get_evaluation(self.session, self.validation_handle,
                                                            self.run_config.train_eval_num, "eval")
                        # test_auc = get_evaluation_all_rw(self.session, self.model, self.test_handle, i_epoch, 'test', self.run_config.pdt_ratio)
                        # val_auc = get_evaluation_all_rw(self.session, self.model, self.validation_handle, i_epoch,
                        #                                 'validation',
                        #                                 self.run_config.pdt_ratio)
                        if is_chief:
                            save_path = self.saver.save(self.graph_handler.get_session(self.session),
                                                        self.train_config.ckpt_dir + '/epoch_' + str(i_epoch),
                                                        global_step=global_step)
                            print("checkpoint saved in path: %s, auc %.4f" % (save_path, val_auc))
                    timer.elapsed()
                    if not self.local_mode and (i_epoch == self.run_config.max_epoch - 1):
                        with wfile.Open(self.train_config.flag_dir + "/" + str(task_index) + '_success', 'w') as f:
                            f.write("done")
                        print("length of flag is:", len(wfile.ListDirectory(self.train_config.flag_dir)), "num_worker:",
                              self.run_config.distribute['sync_replica'])
                        if is_chief:
                            while True:
                                self.try_times += 1
                                if len(wfile.ListDirectory(self.train_config.flag_dir)) >= int(
                                        self.run_config.distribute['sync_replica'] * 0.7):
                                    print("chief worker wait log time for last evaluation")
                                    break
                                elif self.try_times > 5:
                                    print("try 5 times over")
                                    break
                                else:
                                    time.sleep(600)
                    break

        if is_chief:
            # global_step_tensor = training_util._get_or_create_global_step_read(self.session.graph)
            # global_step_all = self.session.run(global_step_tensor)
            save_path = self.saver.save(self.graph_handler.get_session(self.session),
                                        self.train_config.ckpt_dir + '/epoch_' + str(i_epoch + 1),
                                        global_step=max(global_step, self.start_step))
            # ---- dev ----
            val_auc = self.model.get_evaluation(self.session, self.validation_handle, self.run_config.max_epoch,
                                                "eval")

            # is_in_top, deleted_step = perform_recoder.update_top_list(self.steps, val_auc, self.session,
            #                                                           self.graph_handler, self.run_config.tmp_path,
            #                                                           self.steps,
            #                                                           self.train_config.ckpt_dir)
            print("last time checkpoint saved in path: %s" % save_path)
            tf.compat.v1.logging.info(
                '<EPOCH {}>: Finish evaluation, take {} mins, auc reach {}'.format(self.run_config.max_epoch + 1,
                                                                                   timer.elapsed(), val_auc))
            if val_auc > 0.75:
                return 0
            else:
                return 1
        else:
            return 0

    def save_variables(self, save_dir, filetype="txt"):
        # variables = [var.name for var in tf.get_collection(tf.GraphKeys.VARIABLES)]
        # checkpoint_raw = tf.train.get_checkpoint_state(self.train_config.ckpt_dir)
        # ckpt_reader = tf.train.load_checkpoint(checkpoint_raw.model_checkpoint_path)
        # _tensorflow_save_parameters_with_partition(ckpt_reader, variables, self.train_config.online_graph_dir, out_type='txt')

        ckpt = tf.train.get_checkpoint_state(self.train_config.ckpt_dir)
        saver = tf.train.Saver()
        with tf.Session() as sess:
            saver.restore(sess, ckpt.model_checkpoint_path)
            tensorflow_save_parameters_with_partition(sess, save_dir, out_type=filetype,
                                                      tmp_dir=self.run_config.tmp_path)

    def export_model(self, data_config):
        model_dir = os.path.join(self.train_config.log_dir,
                                 'save_predict_model')  # # '/Users/snoworday/data/wide_deep/experiment/2019-02-17/log_data/save_predict_model'
        variable_dir = self.train_config.log_dir

        self.save_variables(variable_dir, "txt")
        self.freezy_model(self.model.out_node, self.train_config.ckpt_dir, model_dir)
        self.model.union_info_weight(self.train_config.id_file_dir, self.train_config.online_graph_dir,
                                     data_config.bucket_info, variable_dir, model_dir,
                                     self.train_config.online_graph_dir)

        if wfile.Exists(self.train_config.export_dir): wfile.DeleteRecursively(self.train_config.export_dir)
        with wfile.Open(os.path.join(self.train_config.online_graph_dir, "UPDATE_LOCK"), "w") as lock_file:
            lock_file.write("done")
        wfile.CopyDirectory(self.train_config.online_graph_dir, self.train_config.export_dir)

    def freezy_model(self, output_node_names, checkpoint_dir, save_predict_dir):
        """
        freezy predict graph
        """
        if self.model.out_node is not None:
            if not wfile.Exists(save_predict_dir):
                wfile.MakeDirs(save_predict_dir)
            checkpoint_raw = tf.train.get_checkpoint_state(checkpoint_dir)
            saver = tf.train.Saver()
            with tf.Session() as sess:
                print('restore sess start')
                # for op in tf.get_default_graph().get_operations():
                #    print (op.name)
                print('last_checkpoint_path', checkpoint_raw.model_checkpoint_path)
                saver.restore(sess, checkpoint_raw.model_checkpoint_path)
                print('restore sess end')
                saver.save(sess, save_predict_dir + '/data.ckpt')

            print('predict model freezy finish')
            checkpoint = tf.train.get_checkpoint_state(save_predict_dir)
            input_checkpoint = checkpoint.model_checkpoint_path
            # We precise the file fullname of our freezed graph
            output_graph = save_predict_dir + "/model.pb"
            # We import the meta graph and retrive a Saver
            saver = tf.train.import_meta_graph(input_checkpoint + '.meta', clear_devices=True)
            graph = tf.get_default_graph()
            input_graph_def = graph.as_graph_def()

            with tf.Session() as sess:
                saver.restore(sess, input_checkpoint)
                print('start input op:')
                # for op in graph.get_operations():
                #    print(op.name)
                #    print(op.values())

                print("start input tenser")
                # for n in input_graph_def.node:
                #    print (n.name)

                output_graph_def = graph_util.convert_variables_to_constants(
                    sess,
                    input_graph_def,
                    output_node_names.split(",")  # We split on comma for convenience
                )
                # # Finally we serialize and dump the output graph to the filesystem
                with tf.gfile.GFile(output_graph, "wb") as f:
                    f.write(output_graph_def.SerializeToString())
                print("%d ops in the final graph." % len(output_graph_def.node))

                print('start output op:')
                for op in output_graph_def.node:
                    print(op.name)

            # copy model.pb
            save_model_path = os.path.join(self.train_config.online_graph_dir, 'model')
            if wfile.Exists(save_model_path): wfile.DeleteRecursively(save_model_path)
            wfile.MakeDirs(save_model_path)
            wfile.Copy(os.path.join(save_predict_dir, "model.pb"), os.path.join(save_model_path, "model.pb"))
